<?php include_once('header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="heading-buttons">
        <h3 class="glyphicons sort"><i></i> <?php echo $title; ?></h3>
        <div class="buttons pull-right">
            <a href="<?php echo $link_arr['add']; ?>" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i> Add page</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="separator"></div>

    <div class="innerLR">
        <table class="table table-bordered table-condensed table-striped table-vertical-center checkboxs js-table-sortable">
            <thead>
                <tr>
                    <th style="width: 1%;" class="uniformjs"><input type="checkbox" /></th>
                    <th style="width: 1%;" class="center">No.</th>
                    <th><?php echo $field_arr['name']; ?></th>
                    <th class="center"><?php echo $field_arr['status']; ?></th>
                    <th class="right" colspan="3">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $key => $value) { ?>
                    <tr class="selectable">
                        <td class="center uniformjs"><input type="checkbox" /></td>
                        <td class="center"><?php echo $value['id']; ?></td>
                        <td><strong><?php echo $value['name']; ?></strong></td>
                        <td class="center important"><?php echo $value['status']; ?></td>
                        <td class="center" style="width: 80px;"><?php echo date('d M Y', strtotime($value['created'])); ?></td>
                        <td class="center" style="width: 60px;">
                            <a href="<?php echo $link_arr['add'] . '?id=' . $value['id']; ?>" class="btn-action glyphicons pencil btn-success"><i></i></a>
                            <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <div class="separator top form-inline small">
            <div class="pull-left checkboxs_actions hide">
                <label class="strong">With selected:</label>
                <select class="selectpicker" data-style="btn-default btn-small">
                    <option>Action</option>
                    <option>Action</option>
                    <option>Action</option>
                </select>
            </div>
            <div class="pagination pull-right" style="margin: 0;">
                <ul>
                    <li class="disabled"><a href="#">&laquo;</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br/>		
    <!-- End Content -->
</div>
<?php include_once('footer.php'); ?>            