<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 100px !important; }
    .form-horizontal .controls { margin-left: 110px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
</style>    
<script>
</script>    
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Primary Lead Process</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="control-group-title">Primary Lead Information</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Territory NO.</label>
                                    <div class="controls">
                                        <select>
                                            <?php echo $data["territory"]; ?>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="Territory Number Required"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date lead received date</label>
                                    <div class="controls">
                                        <input name="leadreceiveddate" id="leadreceiveddate" type="text" style="width:220px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="Date lead received is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Lead received</label>
                                    <div class="controls">
                                        <select>
                                            <?php echo $data["leadreceived"]; ?>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="lead received is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Lead Rec'd By whom</label>
                                    <div class="controls">
                                        <select name="selpersonresponsible" id="selpersonresponsible">
                                            <option value="0">Select Responsible Person</option>
                                            <option value="1">GM</option>
                                            <option value="2">HK</option>
                                            <option value="3">PH</option>
                                            <option value="4">RH</option>
                                            <option value="5">RR</option>
                                            <option value="6">SH</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Lead Source</label>
                                    <div class="controls">
                                        <select name="leadsources1" id="leadsources1">
                                            <option value="-1">Select Lead Source</option>
                                            <option value="0">(Unknown)</option>
                                            <option value="1">Website leads</option>
                                            <option value="2">Google/Internet</option>
                                            <option value="3">Checkatrade</option>
                                            <option value="4">Damp-proofer referral</option>
                                            <option value="5">Existing customer</option>
                                            <option value="6">Landlord Press</option>
                                            <option value="7">Letting Agent</option>
                                            <option value="8">Recommended/Word of Mouth</option>
                                            <option value="9">Rentokil</option>
                                            <option value="10">Social Media</option>
                                            <option value="11">Tradesperson</option>
                                            <option value="12">TV advert</option>
                                            <option value="13">Canvassing</option>
                                            <option value="14">Franchisee</option>
                                            <option value="15">In store promotion</option>
                                            <option value="16">Installer</option>
                                            <option value="17">Landlord Open Day/ Forums</option>
                                            <option value="18">Local Directories</option>
                                            <option value="19">Local Press Advert</option>
                                            <option value="20">National Press Advert</option>
                                            <option value="21">Networking</option>
                                            <option value="22">Professional intermediary</option>
                                            <option value="23">Radio advert</option>
                                            <option value="24">Specialist Support Services</option>
                                            <option value="25">Television Progamme</option>
                                            <option value="26">Trade Show</option>
                                            <option value="27">Van Livery</option>
                                            <option value=""></option>
                                            <option value=""></option>
                                            <option value=""></option>
                                        </select>
                                        <select name="leadsources2" id="leadsources2">
                                        <option value="">Envirovent</option>
                                        <option value="">Ecotechnics</option>
                                        <option value="">Other - Specify</option>
                                        </select>
                                        <select name="leadsources3" id="leadsources3">
                                        <option value=""></option>
                                        </select>
                                        <select name="leadsources4" id="leadsources4">
                                        <option value=""></option>
                                        </select>
                                        
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Lead Source description</label>
                                    <div class="controls">
                                        <textarea name="txtleadsrcdesc" id="txtleadsrcdesc" row="5" cols="10"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group-title">Contact Information for  Lead</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Company Name</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;display:none;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Branch</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Title</label>
                                    <div class="controls">
                                        <select name="dcontacttitle" id="dcontacttitle">
                                            <?php echo contacttitlevalues(); ?>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact First Name</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Last Name</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Phone Number(Primary)</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Phone Number (Secondary)</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Email address (1)</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Email address (2)</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group-title">Billing detail for  Lead</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Billing House Name/Number</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Billing Street</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Billing Street 2</label>
                                    <div class="controls">
                                        <textarea name="txtleadstreet" id="txtleadsrcdesc" row="5" cols="10"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Billing Town</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Billing County</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Billing Postcode</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group-title">Installation Information for  Lead</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation House Name/Number</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation Street</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation Street 2</label>
                                    <div class="controls">
                                        <textarea name="txtleadstreet" id="txtleadsrcdesc" row="5" cols="10"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation Town</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation County</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation Postcode</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group-title">Survey Contact Information for  Lead</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Tenant to arrange Survey</label>
                                    <div class="controls">
                                        <select>
                                            <option value="tenant">tenant</option>
                                            <option value="landlord">landlord</option>
                                            <option value="agent">agent</option>
                                            <option value="none">none</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">First Name</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Last Name</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Number (Home)</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact Number Mobile)</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Commission Payment Required</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Amounts</label>
                                    <div class="controls">
                                        <select>
                                            <option value="0">Select</option>
                                            <option value="1">10% net total</option>
                                            <option value="2">12.5% of PIV and fans</option>
                                            <option value="3">Fixed amount need a box for amount</option>
                                            <option value="4">Other</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Property type</label>
                                    <div class="controls">
                                        <select>
                                            <option value="">House-detached</option>
                                            <option value="">House- semi detached</option>
                                            <option value="">House-terrace-middle</option>
                                            <option value="">House-terrace-end</option>
                                            <option value="">Bungalow-detached</option>
                                            <option value="">Bungalow-semi detached</option>
                                            <option value="">Bungalow-terrace-middle</option>
                                            <option value="">Bungalow-terrace-end</option>
                                            <option value="">Flat -basement</option>
                                            <option value="">Flat -ground</option>
                                            <option value="">Flat -1st floor</option>
                                            <option value="">Flat -2nd floor</option>
                                            <option value="">Flat -3rd floor</option>
                                            <option value="">Flat -4th floor</option>
                                            <option value="">Flat -5th floor</option>
                                            <option value="">Flat -6th floor</option>
                                            <option value="">Maisonette -ground</option>
                                            <option value="">Maisonette -first</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Customer type</label>
                                    <div class="controls">
                                        <select>
                                            <option value="">Private Domestic</option>
                                            <option value="">Independent Landlord</option>
                                            <option value="">NLA Landlord</option>
                                            <option value="">RLA Landlord</option>
                                            <option value="">SLA Landlord</option>
                                            <option value="">Landlord HMO</option>
                                            <option value="">Tenant/parent owned</option>
                                            <option value="">Letting Agency</option>
                                            <option value="">Damp Proofer</option>
                                            <option value="">Building Trades</option>
                                            <option value="">property Management andmaintenance companies</option>
                                            <option value="">Professional Intermediary</option>
                                            <option value="">Trade Intermediary</option>
                                            <option value="">Architect</option>
                                            <option value="">housing association</option>
                                            <option value="">Charity with own housing</option>
                                            <option value="">charity</option> 
                                            <option value="">Trade Supplier</option>
                                            <option value="">New Build Domestic</option>
                                            <option value="">retail  food/drink</option>
                                            <option value="">retail Hairdresser / Beauty Salon</option>
                                            <option value="">Gym / Fitness Centre</option>
                                            <option value="">Hotel / Guest House</option>
                                            <option value="">Dentist</option>
                                            <option value="">Office</option>
                                            <option value="">Other (please specify)</option>
                                            <option value="">(Unknown)</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Azura entry date</label>
                                    <div class="controls">
                                        <input name="Azuraentrydate" id="Azuraentrydate" type="text" style="width:220px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group-title" style="width:90%;">
                                    <div style="float:left;">
                                        Product Information Required
                                    </div>
                                    <div style="float:right;width:200px;">
                                        <input type="button" name="btnaddproducts" id="btnaddproducts" value="Add more product">
                                    </div>    
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">1st Product of Interest</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>    
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">2nd Product of Interest</label>
                                    <div class="controls">
                                        <input type="text" style="width:220px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
            </div>    
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>    
<script>
    $(document).ready(function() {
        $("#leadreceiveddate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $( "#Azuraentrydate" ).datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        var opt1 = "Envirovent,Ecotechnics,Other - Specify";
        var opt2 = "";
        var opt3 = "Ecotechnics pages,Envirovent HQ,Franchisee";
        var opt4 = "New Contact,New Contact From canvassing,Existing lead,Existing relationship,Franchisee";
        var opt5 = "Eco Primary Territory,Envirovent,Other Franchisee";
        var opt6 = "Landlord today,UK Landlord,NLA Other,RLA,SLA,Other - Specify";
        var opt7 = "New Contact,New Contact From canvassing,Existing lead,Existing relationship,Franchisee";
        var opt8 = "Family and Friends,Existing Customer,Professional intermediary,EV staff member - Specify,Specify by whom-input field";
        var opt9 = "On Tracker,Not on Tracker";
        var opt10 = "Facebook,Linkedin,Twitter,Blog";
        var opt11 = "Plasterer,Electrician,Plumber,Window company,Other - Specify";
        var opt12 = "Envirovent,Other - Specify";
        var opt13 = "Letting Agents,Damp Proofers,Door to Door,Other - Specify";
        var opt14 = "Ecotechnics,Nik Alwright,Grant Amos,Richard Evans,Andy Hobbs,Ian Mantrippe,Kim Thorogood,Richard Whiting,Other - Specify";
        var opt15 = "";
        var opt16 = "Envirovent,Ecotechnics";
        var opt17 = "NLA Hastings,NLA Bexley/Bromley/Greenwich,LA Kingston,NLA Dartford,Other NLA Specify,RLA,SLA,Dartford Council,Gravesham Council,Ashford Council,Bromley  Council,Croydon  Council,Sutton  Council,Kingston  Council,West Kent Landlord Forum,S. E. London landlords Forum,Other - Specify";
        var opt18 = "Yellow pages,Yell.com,Thompsom Local,Other - Specify";
        var opt19 = "Newspaper - specify,Community Magazine - specify,Other - Specify";
        var opt20 = "Newspaper Specify,Magazine Specify,Other - Specify";
        var opt21 = "BNI referral,4N,Chamber of Commerce,Other";
        var opt22 = "Independent Surveyor,Other - Specify";
        var opt23 = "Eco,Envirovent,Other - Specify";
        var opt24 = "Family Mosaic,Royal British legion,Other - Specify";
        var opt25 = "Grand Designs,Property Ladder,Phil Spencer Secret agent,The Restoration Man,Other - Specify";
        var opt26 = "Envirovent,Ecotechnics,Franchisee";
        var opt27 = "Eco,Envirovent";
        var opt_3_3 = "Nik Alwright,Grant Amos,Richard Evans,Andy Hobbs,Ian Mantrippe,Kim Thorogood,Richard Whiting,Other - Specify";
        var opt_4_5 = "Nik Alwright,Grant Amos,Richard Evans,Andy Hobbs,Ian Mantrippe,Kim Thorogood,Richard Whiting,Other - Specify";
        var opt_5_1 = "36,40";
        var opt_5_2 = "Nik Alwright,Grant Amos,Richard Evans,Andy Hobbs,Ian Mantrippe,Kim Thorogood,Richard Whiting,Other - Specify";
        var opt_7_5 = "Nik Alwright,Grant Amos,Richard Evans,Andy Hobbs,Ian Mantrippe,Kim Thorogood,Richard Whiting,Other - Specify";
        var opt_8_2 = "Domestic,Landlord,Letting Agent";
        var opt_9_1 = "Enquiry Lead,Surveyor lead";
        var opt_10_1 = "Envirovent,Ecotechnics,Franchisee";
        var opt_10_2 = "Envirovent,Ecotechnics,Other - Specify";
        var opt_10_3 = "Envirovent,Ecotechnics,Other - Specify";
        var opt_10_4 = "Eco,Envirovent,Other - Specify";
        var opt_16_1 = "Dan Walters,Dave Morley,Samir Delhoun,Tom Redmond,Nick Prior,Luke Player,Other - Specify";
        var opt_17_15 = "Tunbridge Wells,Sevenoaks,West Malling,Tonbridge";
        var opt_26_1 = "Ecobuild,Landlord and letting show,National Home building and Rennovating show,Grand Designs Live,Other - Specify";
        var opt_26_2 = "EV paid,Eco paid,EV/Eco shared payment,No payment required,Other - Specify";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        var opt = "";
        $("#leadsources1").change(function(e) {
            //console.log($("#leadsources1 option:selected").val());
            a = eval("opt"+$("#leadsources1 option:selected").val());
            //console.log(a);
            dpv = a.split(",");
            $('#leadsources2').empty();
            $('#leadsources2').hide();
            $('#leadsources3').hide();
            $('#leadsources4').hide();
            $.each(dpv, function(index,value){
                //console.log(index + " : " + value);
                $('#leadsources2').append($("<option></option>").attr("value",index+1).text(value)); 
            });
            $('#leadsources2').show();
        });
        $("#leadsources2").change(function(e) {
            //console.log(("opt_"+$("#leadsources1 option:selected").val())+"_"+$("#leadsources2 option:selected").val());
            a = "opt_"+$("#leadsources1 option:selected").val()+"_"+$("#leadsources2 option:selected").val();
            //console.log(a);
            tval = "";
            $('#leadsources3').hide();
            $('#leadsources4').hide();
            try {
                tval = eval(a);
                dpv = tval.split(",");
                $('#leadsources3').empty();
                $.each(dpv, function(index,value){
                //console.log(index + " : " + value);
                    $('#leadsources3').append($("<option></option>").attr("value",index+1).text(value)); 
                });
                $('#leadsources3').show();
            }
            catch(err) {
                
            }
            //console.log(tval);
        });
    });
</script>