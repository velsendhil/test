<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Reconciliation of Envirovent Payments</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <!--<div class="span6">-->
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Quote number</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Customer Name</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Company Name</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Installation Address</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Date of Sale</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Installation completed date</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Invoice received from Envirovent</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Reconciled on Azura</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Expected payment date from EV</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">Profit due to ECO</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Net</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Vat</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Gross</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">Commission due to Rentokil</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Net</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Vat</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Gross</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">Profit - Commission to Rentokil</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Net</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Vat</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Gross</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Paid - Envirovent Statement Date</label>
                                        <div class="controls">
                                            <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">Amount paid by EV</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Net</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Vat</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Gross</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Additions - Refund Paid separately</label>
                                        <div class="controls">
                                            <select name="selpersonresponsible" id="selpersonresponsible">
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                            </select>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Description of cost</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Deduction taken seperately</label>
                                        <div class="controls">
                                            <select name="selpersonresponsible" id="selpersonresponsible">
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                            </select>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Description of cost</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Amount due</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Description of discrepancy</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Queried with EV</label>
                                        <div class="controls">
                                            <input type="text" name="btnQueriedwithEV" id="btnQueriedwithEV" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Nature of Query</label>
                                        <div class="controls">
                                            <select name="selContactmethod" id="selContactmethod">
                                                <option>Missing Invoice</option>
                                                <option>Unreconciled on Azura</option>
                                                <option>Reconciled on Azura but no payment made</option>
                                                <option>Discrepancy on payment due versus actual</option>
                                                <option>Deduction</option>
                                                <option>Addition</option>
                                            </select>    
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group" style="width: 810px;">
                                        <label class="control-label" for="personresponsible">Details of Query</label>
                                        <div class="controls">
                                            <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Resolved with EV</label>
                                        <div class="controls">
                                            <input type="text" name="btnResolvedwithEV" id="btnResolvedwithEV" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group" style="width: 810px;">
                                        <label class="control-label" for="personresponsible">Notes on Resolution</label>
                                        <div class="controls">
                                            <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Expected payment date from EV</label>
                                        <div class="controls">
                                            <input type="text" name="btnExpectedpaymentdatefromEV" id="btnExpectedpaymentdatefromEV" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <!--<div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls"><span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>-->
                                <!--</div>-->
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>
<script>
    $(document).ready(function() {
        $("#btnInitialcontactdate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnQueriedwithEV").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnResolvedwithEV").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnExpectedpaymentdatefromEV").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-yy'
        });
    });
</script>    