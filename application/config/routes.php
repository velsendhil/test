<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = '';
$route['/'] = "home/index";

// User controller start here...
$route['login.html'] = "user/user/login";
$route['register.html'] = "user/user/register";
$route['profile.html'] = "user/user/profile";
$route['usergroup.html'] = "user/user/usergroup";
$route['add_usergroup.html'] = "user/user/add_usergroup";

// Content controller start here...
$route['home.html'] = "content/content/home";
$route['add_item.html'] = "content/content/add_item";
$route['itemtype.html'] = "content/content/itemtype";
$route['add_itemtype.html'] = "content/content/add_itemtype";
$route['postal.html'] = "content/content/postal";
$route['add_postal.html'] = "content/content/add_postal";
$route['territoty.html'] = "content/content/territory";
$route['add_territory.html'] = "content/content/add_territory";
$route['cost_detail.html'] = "content/content/cost_detail";
$route['payment.html'] = "content/content/payment";

$route['logout.html'] = "user/user/logout";

//$route[''] = "";
//$route['Primary_Lead.html'] = "lead/lead/PrimaryLead";
$route['Lead_Intial.html'] = "lead/lead/LeadIntial";
$route['Lead_Followup_1.html'] = "lead/lead/Lead_Followup_1";
$route['Lead_Followup_2.html'] = "lead/lead/Lead_Followup_2";
$route['Lead_Followup_3.html'] = "lead/lead/Lead_Followup_3";
$route['Lead_Followup_4.html'] = "lead/lead/Lead_Followup_4";
$route['Primary_Lead_Process.html'] = "lead/lead/Primary_Lead_Process";
$route['Survey_Book.html'] = "lead/lead/Survey_Book";
$route['Quote_Followup_1.html'] = "lead/lead/Quote_Followup_1";
$route['Quote_Followup_2.html'] = "lead/lead/Quote_Followup_2";
$route['Quote_Followup_3.html'] = "lead/lead/Quote_Followup_3";
$route['Quote_Followup_4.html'] = "lead/lead/Quote_Followup_4";
$route['Quote_Followup_5.html'] = "lead/lead/Quote_Followup_5";
$route['Quote_Production.html'] = "lead/lead/Quote_Production";
$route['Quote_Cost.html'] = "lead/lead/Quote_Cost";
$route['Process_Installation.html'] = "installation/installation/Process_Installation";
$route['Process_Installation_1.html'] = "installation/installation/Process_Installation_1";
$route['Payment_Followup.html'] = "installation/installation/Payment_Followup";
$route['Payment_Followup_1.html'] = "installation/installation/Payment_Followup_1";
$route['Payment_Followup_2.html'] = "installation/installation/Payment_Followup_2";
$route['Payment_Followup_3.html'] = "installation/installation/Payment_Followup_3";
$route['Payment_Followup_4.html'] = "installation/installation/Payment_Followup_4";
$route['Post_Sales_Activity.html'] = "installation/installation/Post_Sales_Activity";
$route['dashboard.html'] = "projectreports/projectreports/dashboard";
$route['accountsetting.html'] = "projectreports/projectreports/accountsetting";
$route['help.html'] = "projectreports/projectreports/sitehelp";
$route['ordercancel.html'] = "sales/sales/ordercancel";
$route['commissionpayment.html'] = "payment/payment/commissionpayment";
$route['InvoiceReceipt.html'] = "payment/payment/InvoiceReceiptReconciliationEnvirovent";
$route['Reconciliation.html'] = "payment/payment/ReconciliationEnviroventPayments";
$route['CompletionInvoice.html'] = "installation/installation/CompletionInvoice";
$route['returnscredits.html'] = "payment/payment/returnscredits";
$route['WarrantyCallOut.html'] = "payment/payment/WarrantyCallOut";
$route['Servicing.html'] = "payment/payment/Servicing";
$route['Discount_1.html'] = "lead/lead/Discount_1";
$route['Discount_2.html'] = "lead/lead/Discount_2";
$route['Discount_3.html'] = "lead/lead/Discount_3";
$route['Price_Increase.html'] = "lead/lead/Price_Increase";
$route['OrderAgreed_Costs.html'] = "lead/lead/OrderAgreed_Costs";
$route['Survey_rebook.html'] = "lead/lead/Survey_rebook";
/* End of file routes.php */
/* Location: ./application/config/routes.php */