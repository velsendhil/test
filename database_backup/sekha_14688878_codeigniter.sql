-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2014 at 10:34 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sekha_14688878_codeigniter`
--

-- --------------------------------------------------------

--
-- Table structure for table `cutomer_type`
--

CREATE TABLE IF NOT EXISTS `cutomer_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cutomer_type_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `price` varchar(256) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_types`
--

CREATE TABLE IF NOT EXISTS `item_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `item_types`
--

INSERT INTO `item_types` (`id`, `name`, `description`, `created`, `status`) VALUES
(1, 'Item Type 1', 'Item Type 1', '2014-06-12 19:16:46', 'Active'),
(2, 'Mobile', 'Mobile Description', '2014-06-12 19:16:46', 'Active'),
(3, 'Item Type 2', 'Item Type 2', '2014-06-12 19:16:46', 'Active'),
(4, 'ghhhhhhhhhhhhhhhhhhh', 'fjhhhfjhhhhhhhhhh hhhhhhhhhhhhh', '2014-06-12 19:16:46', 'Active'),
(5, 'sac', 'ascas', '2014-06-12 19:16:46', 'Active'),
(6, 'sac ddddss dsaa', 'ascas  ggfdff', '2014-06-12 19:16:46', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `lead_received`
--

CREATE TABLE IF NOT EXISTS `lead_received` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lead_received`
--

INSERT INTO `lead_received` (`id`, `content`, `description`, `status`) VALUES
(1, 'Telephone', 'Telephone', 'Active'),
(2, 'Email', 'Email', 'Active'),
(3, 'Post', 'Post', 'Active'),
(4, 'Tel + Email', 'Tel + Email', 'Active'),
(5, 'Tel + Post', 'Tel + Post', 'Active'),
(6, 'Face to Face', 'Face to Face', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `lead_source`
--

CREATE TABLE IF NOT EXISTS `lead_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `postals`
--

CREATE TABLE IF NOT EXISTS `postals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postal_code` varchar(256) NOT NULL,
  `population` varchar(255) NOT NULL,
  `households` varchar(255) NOT NULL,
  `territory` varchar(255) NOT NULL,
  `additional_information` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `postals`
--

INSERT INTO `postals` (`id`, `postal_code`, `population`, `households`, `territory`, `additional_information`, `created`, `status`) VALUES
(1, '756115', '1200', '555', '888', 'test', '2014-06-12 19:18:37', 'Active'),
(2, '756115', '12345', '1254', '1254', '12544', '2014-06-12 19:27:09', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE IF NOT EXISTS `property_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `property_type_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `territories`
--

CREATE TABLE IF NOT EXISTS `territories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `territories`
--

INSERT INTO `territories` (`id`, `name`, `description`, `created`, `status`) VALUES
(1, '36', '36', '2014-07-01 10:41:08', 'Active'),
(2, '40', '40', '2014-07-01 10:41:40', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('Male','Female') DEFAULT 'Male',
  `age` varchar(255) NOT NULL,
  `about_me` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `last_loggedin` datetime DEFAULT NULL,
  `status` enum('Active','Inactive','Deleted') NOT NULL DEFAULT 'Active',
  `deactivation_reason` text,
  `activation_token` varchar(255) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `firstname`, `lastname`, `username`, `date_of_birth`, `gender`, `age`, `about_me`, `address`, `phone`, `image`, `location`, `website`, `ip`, `last_loggedin`, `status`, `deactivation_reason`, `activation_token`, `password_reset_token`, `created`, `modified`) VALUES
(1, 'chakradhar@gmail.com', '123456', 'chakradhar', 'sahoo123', 'chakra.sahoo', '0000-00-00', 'Male', '', 'I am a cool guy.', '', '', '1396259233chakra.png', NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'msamvel@gmail.com', '123456', 'Vel', 'Sendhil', 'Vel.Sendhil', '0000-00-00', 'Male', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'msamvel@gmail.com', '123456', 'vel', 'sendhil', 'vel.sendhil', '0000-00-00', 'Male', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(256) NOT NULL,
  `group_type` enum('Admin','Super Admin','User') NOT NULL DEFAULT 'Admin',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `group_name`, `group_type`, `created`, `status`) VALUES
(1, 'Group 1', 'Admin', '2014-06-12 19:19:38', 'Active'),
(2, 'test', 'Admin', '2014-06-13 17:00:09', 'Active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
