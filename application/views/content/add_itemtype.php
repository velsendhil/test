<?php include_once('header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="add_itemtype" method="post" autocomplete="off" action="add_itemtype.html">
            <input type='hidden' name="id" id="id" value="<?php echo $id;?>">
            <input type='hidden' name="mode" id="mode" value="<?php echo $mode;?>">
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i><?php echo $mode;?> Item Type</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="name">Name</label>
                                        <div class="controls"><input class="span12" id="name" name="name" type="text" value="<?php if(isset($data['name'])) echo $data['name']; ?>" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="name">Description</label>
                                        <div class="controls"><textarea name="description" id="description" style="width: 310px;height:80px;"><?php if(isset($data['description'])) echo $data['description']; ?></textarea></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="type">Status</label>
                                        <div class="controls">
                                            <select name="status" id="status" style="width:140px;">
                                                <option value="">--Select Status--</option>
                                                <option value="Active" <?php if(isset($data['status']) && $data['status'] == 'Active') echo 'selected'; ?>>Active</option>
                                                <option value="Inactive" <?php if(isset($data['status']) && $data['status'] == 'Inactive') echo 'selected'; ?>>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
                                <a href="itemtype.html" title="Cancel"><button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>