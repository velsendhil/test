<body>
    <!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
    <head>
        <title>Add Item</title>	
        <!-- Meta -->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <!-- Bootstrap -->
        <link href="<?php echo(CSS . 'bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo(CSS . 'bootstrap/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet" />
        <!-- Bootstrap Extended -->
        <link href="<?php echo(CSS . 'bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo(CSS . 'bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo(CSS . 'bootstrap/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css'); ?>" rel="stylesheet">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo(CSS . 'theme/scripts/select2/select2.css'); ?>"/>
        <!-- JQueryUI v1.9.2 -->
        <link rel="stylesheet" href="<?php echo(CSS . 'theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css'); ?>"/>
        <!-- Glyphicons -->
        <link rel="stylesheet" href="<?php echo(CSS . 'theme/css/glyphicons.css'); ?>"/>
        <!-- Bootstrap Extended -->
        <link rel="stylesheet" href="<?php echo(CSS . 'bootstrap/extend/bootstrap-select/bootstrap-select.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo(CSS . 'bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css'); ?>"/>
        <!-- Uniform -->
        <link rel="stylesheet" href="<?php echo(CSS . 'theme/scripts/pixelmatrix-uniform/css/uniform.default.css'); ?>"/>
        <!-- JQuery v1.8.2 -->
        <script src="<?php echo(CSS . 'theme/scripts/jquery-1.8.2.min.js'); ?>"></script>
        <!-- Modernizr -->
        <script src="<?php echo(CSS . 'theme/scripts/modernizr.custom.76094.js'); ?>"></script>
        <!-- MiniColors -->
        <link rel="stylesheet" href="<?php echo(CSS . 'theme/scripts/jquery-miniColors/jquery.miniColors.css'); ?>"/>
        <!-- Theme -->
        <link rel="stylesheet" href="<?php echo(CSS . 'theme/css/style.min.css?1363272449'); ?>"/>
        <!-- LESS 2 CSS -->
        <script src="<?php echo(CSS . 'theme/scripts/less-1.3.3.min.js'); ?>"></script>
    </head>
    <body>    
        <!-- Start Content -->
        <div class="container-fluid fixed login">
            <div class="navbar main">
                <a href="home.html" class="appbrand"><span>Admin+ <span>lovely headline here</span></span></a>
                <ul class="topnav pull-right">
                    <li class="visible-desktop">
                        <ul class="notif">
                            <li><a href="#" class="glyphicons envelope" data-toggle="tooltip" data-placement="bottom" data-original-title="5 new messages"><i></i> 5</a></li>
                            <li><a href="#" class="glyphicons shopping_cart" data-toggle="tooltip" data-placement="bottom" data-original-title="1 new orders"><i></i> 1</a></li>
                            <li><a href="#" class="glyphicons log_book" data-toggle="tooltip" data-placement="bottom" data-original-title="3 new activities"><i></i> 3</a></li>
                        </ul>
                    </li>
<!--                    <li class="dropdown visible-desktop">
                        <a href="" data-toggle="dropdown" class="glyphicons cogwheel"><i></i>Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="">Some option</a></li>
                            <li><a href="">Some other option</a></li>
                            <li><a href="">Other option</a></li>
                        </ul>
                    </li>
                    <li class="hidden-phone">
                        <a href="#themer" data-toggle="collapse" class="glyphicons eyedropper"><i></i><span>Themer</span></a>
                        <div id="themer" class="collapse">
                            <div class="wrapper">
                                <span class="close2">&times; close</span>
                                <h4>Themer <span>color options</span></h4>
                                <ul>
                                    <li>Theme: <select id="themer-theme" class="pull-right"></select><div class="clearfix"></div></li>
                                    <li>Primary Color: <input type="text" data-type="minicolors" data-default="#ffffff" data-slider="hue" data-textfield="false" data-position="left" id="themer-primary-cp" /><div class="clearfix"></div></li>
                                    <li>
                                        <span class="link" id="themer-custom-reset">reset theme</span>
                                        <span class="pull-right"><label>advanced <input type="checkbox" value="1" id="themer-advanced-toggle" /></label></span>
                                    </li>
                                </ul>
                                <div id="themer-getcode" class="hide">
                                    <hr class="separator" />
                                    <button class="btn btn-primary btn-small pull-right btn-icon glyphicons download" id="themer-getcode-less"><i></i>Get LESS</button>
                                    <button class="btn btn-inverse btn-small pull-right btn-icon glyphicons download" id="themer-getcode-css"><i></i>Get CSS</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </li>-->
                    <li class="hidden-phone">
                        <a href="#" data-toggle="dropdown"><img src="public/theme/images/lang/en.png" alt="en" /></a>
                        <ul class="dropdown-menu pull-right">
                            <li class="active"><a href="?page=login&amp;lang=en" title="English"><img src="public/theme/images/lang/en.png" alt="English"> English</a></li>
                            <li><a href="?page=login&amp;lang=ro" title="Romanian"><img src="public/theme/images/lang/ro.png" alt="Romanian"> Romanian</a></li>
                            <li><a href="?page=login&amp;lang=it" title="Italian"><img src="public/theme/images/lang/it.png" alt="Italian"> Italian</a></li>
                            <li><a href="?page=login&amp;lang=fr" title="French"><img src="public/theme/images/lang/fr.png" alt="French"> French</a></li>
                            <li><a href="?page=login&amp;lang=pl" title="Polish"><img src="public/theme/images/lang/pl.png" alt="Polish"> Polish</a></li>
                        </ul>
                    </li>
                    <li class="account">
                        <a href="login.html?lang=en" class="glyphicons logout lock"><span class="hidden-phone text">Welcome <strong>guest</strong></span><i></i></a>
                    </li>
                </ul>
            </div>
            <!--Middle part start.....-->
            <div id="wrapper">
                <div id="menu" class="hidden-phone">
                    <div id="menuInner">
                        <div id="search">
                            <input type="text" placeholder="Quick search ..." />
                            <button class="glyphicons search"><i></i></button>
                        </div>
                        <ul>
                            <li class="heading"><span>Category</span></li>
                            <li class="glyphicons home"><a href="home.html"><i></i><span>Dashboard</span></a></li>
                            <li class="glyphicons charts"><a href="#"><i></i><span>Charts</span></a></li>
                            <li class="hasSubmenu">
                                <a data-toggle="collapse" class="glyphicons show_thumbnails_with_lines collapsed" href="#menu_forms"><i></i><span>Forms</span></a>
                                <ul class="collapse in" id="menu_forms">
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="home.html"><span>Dashboard</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'profile') echo ' active';?>"><a href="profile.html"><span>My Profiles</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'register') echo ' active';?>"><a href="register.html"><span>New User</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'usergroup') echo ' active';?>"><a href="usergroup.html"><span>User Group</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'add_item') echo ' active';?>"><a href="add_item.html"><span>Item Add</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'postal') echo ' active';?>"><a href="postal.html"><span>Postal Master</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'itemtype') echo ' active';?>"><a href="itemtype.html"><span>Item Type master</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'territory') echo ' active';?>"><a href="territoty.html"><span>Territory Master</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'territory') echo ' active';?>"><a href="cost_detail.html"><span>Cost Calculation</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'territory') echo ' active';?>"><a href="payment.html"><span>Payment Method</span></a></li>
                                </ul>
                            </li>
                            <li class="hasSubmenu">
                                <a data-toggle="collapse" class="glyphicons show_thumbnails_with_lines collapsed" href="#lead_forms"><i></i><span>Lead Stage</span></a>
                                <ul class="collapse in" id="lead_forms">
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Primary_Lead_Process.html"><span>Primary Lead process</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'profile') echo ' active';?>"><a href="Lead_Intial.html"><span>Lead initial change to Initial lead</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'register') echo ' active';?>"><a href="Lead_Followup_1.html"><span>Lead followup 1st</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'usergroup') echo ' active';?>"><a href="Lead_Followup_2.html"><span>Lead followup 2nd</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'add_item') echo ' active';?>"><a href="Lead_Followup_3.html"><span>Lead followup 3rd</span></a></li>
                                </ul>
                            </li>
                            <li class="hasSubmenu">
                                <a data-toggle="collapse" class="glyphicons show_thumbnails_with_lines collapsed" href="#survey_stage"><i></i><span>Survey Stage</span></a>
                                <ul class="collapse in" id="survey_stage">
                                    <li class="<?php if(isset($method_name) && $method_name == 'postal') echo ' active';?>"><a href="Survey_Book.html"><span>Survey Stage</span></a></li>
                                </ul>
                            </li>
                            <li class="hasSubmenu">
                                <a data-toggle="collapse" class="glyphicons show_thumbnails_with_lines collapsed" href="#quote_stage"><i></i><span>Quote Stage</span></a>
                                <ul class="collapse in" id="quote_stage">
                                    <li class="<?php if(isset($method_name) && $method_name == 'itemtype') echo ' active';?>"><a href="Quote_Production.html"><span>Quote Production</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'territory') echo ' active';?>"><a href="Quote_Followup_1.html"><span>Quote Followup 1st</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'territory') echo ' active';?>"><a href="Quote_Followup_2.html"><span>Quote Followup 2nd</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'territory') echo ' active';?>"><a href="Quote_Followup_3.html"><span>Quote Followup 3rd</span></a></li>
                                </ul>
                            </li>
                            <li class="hasSubmenu active">
                                <a data-toggle="collapse" class="glyphicons show_thumbnails_with_lines collapsed" href="#installation_forms"><i></i><span>Installation</span></a>
                                <ul class="collapse in" id="installation_forms">
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Process_Installation.html"><span>Process Installation</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Payment_Followup.html"><span>Payment Followup</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Payment_Followup_1.html"><span>Payment Followup 1</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Payment_Followup_2.html"><span>Payment Followup 2</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Payment_Followup_3.html"><span>Payment Followup 3</span></a></li>
                                    <li class="<?php if(isset($method_name) && $method_name == 'home') echo ' active';?>"><a href="Post_Sales_Activity.html"><span>Post Sales Activity</span></a></li>
                                </ul>
                            </li>    
                        </ul>
                        <ul>
                            <li class="heading"><span>Sections</span></li>
                            <li class="glyphicons sort"><a href="#"><i></i><span>Site Pages</span></a></li>
                            <li class="glyphicons picture"><a href="#"></i><span>Photo Gallery</span></a></li>
                            <li class="glyphicons picture"><a href="logout.html"></i><span>Logout</span></a></li>
                        </ul>
                    </div>
                </div>
