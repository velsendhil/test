<?php include_once(MSTPATH.'header.php'); ?>
<style>
    .form-horizontal .controls {width:400px;}
</style>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>3rd Lead Followup</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Outcome detail</label>
                                    <div class="controls">
                                        <?php echo "This is out come of intial contact"; ?>
                                        <br/>
                                        <?php echo "This is out come of 1st Followup"; ?>
                                        <br/>
                                        <?php echo "This is out come of 2nd Followup"; ?>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Person Responsible</label>
                                    <div class="controls">
                                        <select name="selpersonresponsible" id="selpersonresponsible">
                                            <option value="0">Select Responsible Person</option>
                                            <option value="1">GM</option>
                                            <option value="2">HK</option>
                                            <option value="3">PH</option>
                                            <option value="4">RH</option>
                                            <option value="5">RR</option>
                                            <option value="6">SH</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Initial contact date</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Initial contact time</label>
                                    <div class="controls">
                                        <select>
                                            <option>Morning</option>
                                            <option>Afternoon</option>
                                            <option>Evening</option>
                                            <option>Weekend</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contact method</label>
                                    <div class="controls">
                                        <select>
                                            <option>Telephone</option>
                                            <option>Email</option>
                                            <option>Post</option>
                                            <option>Tel + Email</option>
                                            <option>Tel + Post</option>
                                            <option>Face to Face</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Person Contacted</label>
                                    <div class="controls">
                                        <select name="selpersonresponsible" id="selpersonresponsible">
                                            <option value="0">Select Responsible Person</option>
                                            <option value="1">GM</option>
                                            <option value="2">HK</option>
                                            <option value="3">PH</option>
                                            <option value="4">RH</option>
                                            <option value="5">RR</option>
                                            <option value="6">SH</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Achieved contact</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Outcome</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible">Details of Outcome</label>
                                    <div class="controls">
                                        <textarea rows="3" cols="30" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Next followup date</label>
                                    <div class="controls">
                                        <input type="text" name="btnNextfollowupdate" id="btnNextfollowupdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Next followup time</label>
                                    <div class="controls">
                                        <select>
                                            <option>Morning</option>
                                            <option>Afternoon</option>
                                            <option>Evening</option>
                                            <option>Weekend</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Priority followup</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <!--<div class="control-group">
                                    <label class="control-label" for="personresponsible"></label>
                                    <div class="controls"><span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>    
<script>
    $(document).ready(function() {
        $("#btnInitialcontactdate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnNextfollowupdate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });        
    });
</script>    