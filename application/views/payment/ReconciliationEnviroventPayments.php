<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Post Sales Stage</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <!--<div class="span6">-->
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Quote number</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Customer Name</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Company Name</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Installation Address</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Date of Sale</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Installation completed date</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Final payment date</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Invoice received from Envirovent</label>
                                        <div class="controls">
                                            <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Reconciled on Azura</label>
                                        <div class="controls">
                                            <input type="text" name="btnReconciledAzura" id="btnReconciledAzura" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Expected payment date from EV</label>
                                        <div class="controls">
                                            <input type="text" name="btnNextfollowupdate" id="btnNextfollowupdate" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <!--<div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls"><span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>-->
                                <!--</div>-->
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>
<script>
    $(document).ready(function() {
        $("#btnInitialcontactdate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnReconciledAzura").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnNextfollowupdate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-yy'
        });
    });
</script>    