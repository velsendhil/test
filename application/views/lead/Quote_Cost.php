<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 100px !important; }
    .form-horizontal .controls { margin-left: 110px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Quote Costs</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                Under Construction
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>
<script language="Javascript" type="text/javascript">
    function saleofsurvay()
    {
        $("#saleofsurvay_a").hide();
        $("#saleofsurvay_b").hide();
        if ($("#selsaleofsurvay").val() == 5)
        {
            $("#saleofsurvay_a").show();
            $("#saleofsurvay_b").show();
        }
    }
</script>
