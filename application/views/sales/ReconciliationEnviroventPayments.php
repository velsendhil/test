<?php include_once(MSTPATH.'header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="add_item" method="post" autocomplete="off" action="add_item.html">
            <input type='hidden' name="id" id="id" value="<?php echo $id;?>">
            <input type='hidden' name="mode" id="mode" value="<?php echo $mode;?>">
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i><?php echo $mode;?> Item</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="name">Name</label>
                                        <div class="controls"><input class="span12" id="name" name="name" type="text" value="<?php if(isset($data['name'])) echo $data['name']; ?>" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="name">Description</label>
                                        <div class="controls"><textarea name="description" id="description" style="width: 310px;height:80px;"><?php if(isset($data['description'])) echo $data['description']; ?></textarea></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="type">Type</label>
                                        <div class="controls">
                                            <select class="selectpicker span6" data-style="btn-default" name="item_type_id" id="item_type_id" style="width:184px;">
                                                <option value="">--Select Type--</option>
                                                <?php foreach ($item_data as $key => $value) {
                                                        if(isset($data['item_type_id']) && $data['item_type_id'] == $value['id']) {
                                                            $selected = 'selected';
                                                        } else {
                                                            $selected = '';
                                                        }
                                                    echo '<option value="'. $value['id']. '" ' . $selected . '>'. $value['name'].'</option>';    
                                                } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="price">Price</label>
                                        <div class="controls"><input class="span12" id="price" name="price" type="text" value="<?php if(isset($data['price'])) echo $data['price']; ?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>