<?php include_once(MSTPATH.'header.php'); ?>
<!--<div class="span6">
    This is help page
</div>-->
<!-- /span6 -->
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>warranty call out input</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date fault reported</label>
                                    <div class="controls">
                                        <input type="text" name="btnCustomerReportsProductnotworking" id="btnCustomerReportsProductnotworking" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is this the first Warranty Call Out</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date of previous WCO</label>
                                    <div class="controls">
                                        <input type="text" name="btnCustomerReportsProductnotworking" id="btnCustomerReportsProductnotworking" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date of previous WCO</label>
                                    <div class="controls">
                                        <input type="text" name="btnCustomerReportsProductnotworking" id="btnCustomerReportsProductnotworking" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date of previous WCO</label>
                                    <div class="controls">
                                        <input type="text" name="btnCustomerReportsProductnotworking" id="btnCustomerReportsProductnotworking" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Result of visit</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Unit investigated</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">ECO2 Loft unit</option>
                                            <option value="">ECO2 Air unit</option>
                                            <option value="">ECO2 Twin unit</option>
                                            <option value="">ECO2 Air Twin unit</option>
                                            <option value="">ECO2 Wall unit</option>
                                            <option value="">Cyclone 230V</option>
                                            <option value="">Cyclone SELV</option>
                                            <option value="">Cyclone In Line with IQ Control</option>
                                            <option value="">HeatSava Range - Specify</option>
                                            <option value="">Cyclone 7 -230V</option>
                                            <option value="">Cyclone 7-SELV</option>
                                            <option value="">Cyclone 7 In Line with IQ Control</option>
                                            <option value="">Cyclone 7 230V with IQ Control</option>
                                            <option value="">Cyclone 7 SELV with IQ Control</option>
                                            <option value="">EFHT2S Range - Specify</option>
                                            <option value="">Aerodri Range - Specify</option>
                                            <option value="">Silent Range - Specify</option>
                                            <option value="">Ecodmev Range - Specify</option>
                                            <option value="">MVHR Range - Specify</option>
                                            <option value="">Other - Specify</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Setting changed</label>
                                    <div class="controls">
                                        <select>
                                            <option>Small</option>
                                            <option>Medium</option>
                                            <option>Large</option>
                                            <option>Boost</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Azura Quote Number</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Territory Number</label>
                                    <div class="controls">
                                        <select>
                                            <option>36</option>
                                            <option>40</option>
                                            <option>Other - Specify</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Customer Type</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Company Name</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Customer Details</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">Title</option>
                                            <option value="">First Name</option>
                                            <option value="">Surname</option>
                                            <option value="">Contact Number Primary</option>
                                            <option value="">Contact Number Secondary</option>
                                            <option value="">E mail address</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">If Installed</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">Installation House Number or Name</option>
                                            <option value="">Installation Street 1</option>
                                            <option value="">Installation Street 2</option>
                                            <option value="">Installation Town</option>
                                            <option value="">Installation County</option>
                                            <option value="">Installation Post Code</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">If Delivered</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">Delivery House Number or Name</option>
                                            <option value="">Delivery Street 1</option>
                                            <option value="">Delivery Street 2</option>
                                            <option value="">Delivery Town</option>
                                            <option value="">Delivery County</option>
                                            <option value="">delivery Post Code</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Original Survey Date</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Surveyed by</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Units Recommended</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installation Date</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installed by</label>
                                    <div class="controls">
                                        <select>
                                            <option>Envirovent</option>
                                            <option>Ecotechnics</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group" style="width: 810px;">
                                    <label class="control-label" for="personresponsible">Access to the Property Information</label>
                                    <div class="controls">
                                        <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Access via</label>
                                    <div class="controls">
                                        <select>
                                            <option>Owner</option>
                                            <option>Landlord</option>
                                            <option>Tenant</option>
                                            <option>Agent</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Tenant Details</label>
                                    <div class="controls">
                                        Title : 
                                        First Name : 
                                        Surname : 
                                        Contact Number Primary : 
                                        E mail address : 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Agent Details</label>
                                    <div class="controls">
                                        Company name :  
                                        First Name : 
                                        Surname : 
                                        Contact Number Primary : 
                                        E mail address : 
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Photos Taken</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Attached</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Condition of mould/condensation at time of survey</label>
                                    <div class="controls">
                                        <select>
                                            <option>mild</option>
                                            <option>moderate</option>
                                            <option>severe</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">Questions for the client</div>
                                <div style="width:100%;float:left;">What is the reason for the revisit</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Unit not working</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Condensation still occuring</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Mould still occuring</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Moisture dripping from the unit</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Moisture dripping from the ducting</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Other _Specify</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">PIV Fault</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible"></label>
                                    <div class="controls">
                                        <select>
                                            <option>ECO2 Loft unit</option>
                                            <option>ECO2 Air unit</option>
                                            <option>ECO2 Twin unit</option>
                                            <option>ECO2 Air Twin unit</option>
                                            <option>ECO2 Wall unit</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Outcome</label>
                                    <div class="controls">
                                        Other fields in excel sheets are not clear how to display
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Has ducting been installed</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is Boxing In installed</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Fan Fault</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible"></label>
                                    <div class="controls">
                                        <select>
                                            <option>Cyclone 230V</option>
                                            <option>Cyclone SELV</option>
                                            <option>Cyclone In Line with IQ Control</option>
                                            <option>Cyclone 230V with IQ Control</option>
                                            <option>Cyclone SELV with IQ Control</option>
                                            <option>HeatSava</option>
                                            <option>Cyclone 7 -230V</option>
                                            <option>Cyclone 7-SELV</option>
                                            <option>Cyclone 7 In Line with IQ Control</option>
                                            <option>Cyclone 7 230V with IQ Control</option>
                                            <option>Cyclone 7 SELV with IQ Control</option>
                                            <option>Aerodri</option>
                                            <option>Silent Range - Specify</option>
                                            <option>Ecodmev</option>
                                            <option>Other - Specify</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Outcome</label>
                                    <div class="controls">
                                        Other fields in excel sheets are not clear how to display
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">Customer Checks Undertaken</div>
                                <div style="width:100%;float:left;">Problem with PIV Unit</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is the unit still switched on</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Has the fuse been checked</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">If the unit can be accessed easily can the lights be seen on the control panel</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>Cant access unit</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">Problem with Fan</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is there a pull cord</label>
                                    <div class="controls">
                                        <select>
                                            <option>Does it activate the fan - working</option>
                                            <option>Doesn't activate the fan - not working</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is there any evidence of power to the fan</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">Any IQ Unit</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">If an IQ devise is used have the batteries in the remote control been checked</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">General</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Have you had any other building or electrical work carried out in the property since the unit was installed</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible">Details of Outcome</label>
                                    <div class="controls">
                                        <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Has the number of people at the address increased since the unit was installed</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>    
<?php include_once(MSTPATH.'footer.php'); ?>