<?php include_once('header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="add_postal" method="post" autocomplete="off" action="add_postal.html">
            <input type='hidden' name="id" id="id" value="<?php echo $id;?>">
            <input type='hidden' name="mode" id="mode" value="<?php echo $mode;?>">
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i><?php echo $mode;?> Postal</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="postal_code">Postal Code</label>
                                        <div class="controls"><input class="span12" id="postal_code" name="postal_code" type="text" value="<?php if(isset($data['postal_code'])) echo $data['postal_code']; ?>" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="population">Population</label>
                                        <div class="controls"><input class="span12" id="population" name="population" type="text" value="<?php if(isset($data['population'])) echo $data['population']; ?>" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="households">HouseHolds</label>
                                        <div class="controls"><input class="span12" id="households" name="households" type="text" value="<?php if(isset($data['households'])) echo $data['households']; ?>" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="territory">Territory</label>
                                        <div class="controls"><input class="span12" id="territory" name="territory" type="text" value="<?php if(isset($data['territory'])) echo $data['territory']; ?>" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="add_info">Additional Information</label>
                                        <div class="controls"><textarea id="additional_information" name="additional_information" style="width: 310px;height:80px;"><?php if(isset($data['additional_information'])) echo $data['additional_information']; ?></textarea></div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
                                <a href="postal.html" title="Cancel"><button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>