<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Item_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->main_table = "items";
        $this->primary_key = "id";
        $this->title = "Items";
    }

    public function validate() {
        $this->load->library('form_validation');
        // set validate ruls
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'comments',
                'label' => 'Comments',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }

    function insert($data = array(), $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->insert($table, $data);
        $insertId = $this->db->insert_id();
        return $insertId; 
    }

    function update($data = array(), $id='', $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $insertId = $this->db->affected_rows();
        #echo $this->db->last_query();
        return $id;
    }

    function delete($where = '', $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->where($where);
        return $this->db->delete($table);
    }
    
    function getRecords($id, $table, $field="*") {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = array();
        $result = $query->row_array();
        return $result;
    }

}