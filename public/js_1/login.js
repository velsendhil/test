$(document).ready(function(){
    $("#validateSubmitForm").validate({
        rules: {
            user_name: {
                required: true,
                email: true
            },
            user_pass: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            user_name: {
                required: "Please enter a email address",
                email: "Please enter a valid email address"
            },
            user_pass: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            }
        },
        errorPlacement: function(error, element) {
            error.appendTo('#Err' + element.attr('id'));
        }
    });
});