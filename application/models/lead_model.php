<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lead_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->main_table = "users";
        $this->primary_key = "id";
        $this->title = "User";
    }
    public function getTerritory() {
        $this->db->select("id,name");
        $this->db->where("status","Active");
        $this->db->from("territories");
        $query = $this->db->get();
        $result = array();
        if ($query->num_rows() > 0)
        {
            foreach($query->result() as $data)
            {
                $result[] = $data;
            }
        }
        return $result;
    }
    public function getleadreceived() {
        $this->db->select("id,content");
        $this->db->where("status","Active");
        $this->db->from("lead_received");
        $query = $this->db->get();
        $result = array();
        if ($query->num_rows() > 0)
        {
            foreach($query->result() as $data)
            {
                $result[] = $data;
            }
        }
        return $result;
    }
}