<?php include_once(MSTPATH.'header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>PAYMENT - 1st Follow-up (Completion Payment)</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="firstname">Comments</label>
                                        <div class="controls">
                                            <?php echo "To display notes from Payment Followup"; ?>
                                            <br/>
                                            <?php echo "To display notes from Payment 1st Followup"; ?>
                                            <br/>
                                            <?php echo "To display notes from Payment 2nd Followup"; ?>
                                            <br/>
                                            <?php echo "To display notes from Payment 3rd Followup"; ?>
                                            <br/>
                                            <?php echo "To display notes from Payment 4th Followup"; ?>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Date completion invoice sent to customer</label>
                                        <div class="controls">
                                            <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Date of follow up</label>
                                        <div class="controls">
                                            <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Date Final Payment Received from the Customer</label>
                                        <div class="controls">
                                            <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Amount Paid</label>
                                        <div class="controls">
                                            <input type="text" style="width:100px;">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Type of Payment</label>
                                        <div class="controls">
                                            <select>
                                                <option>Deposit</option>
                                                <option>Full</option>
                                            </select>    
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Payment Method</label>
                                        <div class="controls">
                                            <select>
                                                <option>Credit card</option>
                                                <option>Debit card</option>
                                                <option>Cheque</option>
                                                <option>BACS</option>
                                            </select>    
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Misc. Notes on Payments</label>
                                        <div class="controls">
                                            <input type="text" style="width:100px;">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Agreed Date for next follow up of Payment</label>
                                        <div class="controls">
                                            <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>    