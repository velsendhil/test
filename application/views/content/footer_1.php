</div>
<!--Middle part end.....-->
</div>
</body>
</html>
<!-- JQueryUI v1.9.2 -->
<script src="<?php echo(CSS . 'theme/scripts/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js'); ?>"></script>

<!-- JQueryUI Touch Punch -->
<!-- small hack that enables the use of touch events on sites using the jQuery UI user interface library -->
<script src="<?php echo(CSS . 'theme/scripts/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'); ?>"></script>

<!-- MiniColors -->
<script src="<?php echo(CSS . 'theme/scripts/jquery-miniColors/jquery.miniColors.js'); ?>"></script>

<!-- Select2 -->
<script src="<?php echo(CSS . 'theme/scripts/select2/select2.js'); ?>"></script>

<!-- Themer -->
<script>
    var themerPrimaryColor = '#DA4C4C';
</script>
<script src="<?php echo(CSS . 'theme/scripts/jquery.cookie.js'); ?>"></script>
<script src="<?php echo(CSS . 'theme/scripts/themer.js'); ?>"></script>

<!-- jQuery Validate -->
<script src="<?php echo(CSS . 'theme/scripts/jquery-validation/dist/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo(JS . 'login.js'); ?>" type="text/javascript"></script>


<!-- Resize Script -->
<script src="<?php echo(CSS . 'theme/scripts/jquery.ba-resize.js'); ?>"></script>

<!-- Uniform -->
<script src="<?php echo(CSS . 'theme/scripts/pixelmatrix-uniform/jquery.uniform.min.js'); ?>"></script>

<!-- Bootstrap Script -->
<script src="<?php echo(CSS . 'bootstrap/js/bootstrap.min.js'); ?>"></script>

<!-- Bootstrap Extended -->
<script src="<?php echo(CSS . 'bootstrap/extend/bootstrap-select/bootstrap-select.js'); ?>"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js'); ?>"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js'); ?>"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/jasny-bootstrap/js/jasny-bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/jasny-bootstrap/js/bootstrap-fileupload.js'); ?>" type="text/javascript"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/bootbox.js'); ?>" type="text/javascript"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/bootstrap-wysihtml5/js/wysihtml5-0.3.0_rc2.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo(CSS . 'bootstrap/extend/bootstrap-wysihtml5/js/bootstrap-wysihtml5-0.0.2.js'); ?>" type="text/javascript"></script>

<!-- Custom Onload Script -->
<script src="<?php echo(CSS . 'theme/scripts/load.js'); ?>"></script>

<!-- Layout Options -->
<script src="<?php echo(CSS . 'theme/scripts/layout.js'); ?>"></script>