<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Commission payments to third parties</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <!--<div class="span6">-->
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Method of Payment</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Bank sort code</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Bank Account number</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Reference No /description</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">name of payee</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Address for cheque to be sent</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div style="width:100%;float:left;">Commision Payable to</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">name of individual</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">name of organisation</label>
                                        <div class="controls">&nbsp;</div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Rate of Commission Payment due</label>
                                        <div class="controls">
                                            <select name="selpersonresponsible" id="selpersonresponsible">
                                                <option value="1">10% net price</option>
                                                <option value="2">12.5% net price</option>
                                                <option value="3">Fixed amount</option>
                                                <option value="4">other input</option>
                                            </select>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Actual Commission Payable</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Commission paid date</label>
                                        <div class="controls">
                                            <input type="text" name="btnCommissionpaiddate" id="btnCommissionpaiddate" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Commission for rentokil deducted from EV Statement</label>
                                        <div class="controls">
                                            <input type="text" name="btnCommissionforrentokildeductedfromEVStatement" id="btnCommissionforrentokildeductedfromEVStatement" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Date payment debited Eco Account</label>
                                        <div class="controls">
                                            <input type="text" name="btnDatepaymentdebitedEcoAccount" id="btnDatepaymentdebitedEcoAccount" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <!--<div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls"><span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>-->
                                <!--</div>-->
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>
<script>
    $(document).ready(function() {
        $("#btnCommissionpaiddate").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnCommissionforrentokildeductedfromEVStatement").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnDatepaymentdebitedEcoAccount").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
    });
</script>    