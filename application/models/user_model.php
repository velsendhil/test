<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->main_table = "users";
        $this->primary_key = "id";
        $this->title = "User";
    }

    public function validate() {
        $this->load->library('form_validation');
        // set validate ruls
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'comments',
                'label' => 'Comments',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        return $this->form_validation->run();
    }

    function insert($data = array(), $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->insert($table, $data);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

    function update($data = array(), $id='', $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $insertId = $this->db->affected_rows();
        #echo $this->db->last_query();
        return $id;
    }

    function delete($where = '', $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->where($where);
        return $this->db->delete($table);
    }

    function getAuthenicatedUser( $username='', $password='', $field='' ) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $username);
        $this->db->where('password', $password);
        $query = $this->db->get();
        $db_user = $query->row_array();
        //echo $this->db->last_query();exit;
        $ret = $this->setLoginSession($db_user);
        return $ret;
    }

    function getAuthenicatedUserActive( $email, $password ) {
        $db_user = $this->get("`vEmail` = '" . $email . "' AND `vPassword` = '" . $password . "' AND `eStatus` = 'Active' ", "iCustomerId,vEmail");
        return $db_user;
    }

    function logInOutEntry( $id ) {
        $exists = false;
        $user_iloginid = $this->session->read($this->config->session_prefix . 'user_iLogId');
        if ($user_iloginid != '') {
            $sql = "update log_history set dLogoutDate = '" . $this->date->getDateTime() . "' where iLogId='" . $user_iloginid . "'";
            $log_id = $this->db->query($sql);
            $exists = true;
        } else {
            $sql = "insert into log_history (iUserId,vIP,dLoginDate,eUserType ) values ('" . $id . "','" . $_SERVER[REMOTE_ADDR] . "', '" . $this->date->getDateTime() . "','Member')";
            $log_id = $this->db->query($sql);
            $this->session->write($this->config->session_prefix . 'user_iLogId', $log_id);
            $exists = true;
        }
        return $exists;
    }

    function setLoginSession($userarr) {
        if (count($userarr) > 0) {
            $success[] = $userarr;
           // echo "<pre>";print_r($success);exit;
            if ($success[0]['status'] == 'Active') {
                //   $this->logInOutEntry($success[0]['iUserID']);
                $this->session->set_userdata('id', $success[0]['id']);
                $this->session->set_userdata('login_user_id', $success[0]['id']);
                $this->session->set_userdata('firstname', $success[0]['firstname']);
                $this->session->set_userdata('lastname', $success[0]['lastname']);
                $Name = $success[0]['firstname'] . ' ' . $success[0]['lastname'];
                $this->session->set_userdata('name', $Name);
                $this->session->set_userdata('email', $success[0]['email']);
                $this->session->set_userdata('username', $success[0]['username']);
                $this->session->set_userdata('status', $success[0]['status']);
                $data['last_loggedin'] = date('Y-m-d H:i:s');
                $where_cond = "id ='" . $success[0]['id'] . "'";
                $this->update($data, $where_cond);
                $ret['success'] = 'Yes';
                $ret['Status'] = $success[0]['status'];
            } elseif ($success[0]['status'] == 'Inactive') {
                $ret['success'] = 'No';
                $ret['Status'] = 'Inactive';
            } else {
                $ret['success'] = 'No';
                $ret['Status'] = '';
            }
        } else {
            $ret['success'] = 'No';
            $ret['Status'] = '';
        }
        return $ret;
    }

    public function send() {
        $ip = $this->input->ip_address();
        $name = $this->input->post('name', TRUE);
        $email = $this->input->post('email', TRUE);
        $comments = "IP: {$ip}\n\n{$this->input->post('comments', TRUE)}";

        $this->load->library('email');

        $this->email->from($email, $name);

        // please do change this to your testing email account
        $this->email->to('demo@dreamerslab.com');
        $this->email->subject("Demo Site Comments");
        $this->email->message($comments);
        return $this->email->send();
    }

    function get($id) {
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = array();
        $result = $query->row_array();
        return $result;
    }

    function getLoginDetails($username, $password) {
        $this->load->database();
        $this->db->select("id, username, email, firstname, lastname");
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get();
        $result = array();
        $result = $query->row_array();
        return $result;
    }
    
    function getRecords($id, $table, $field="*") {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = array();
        $result = $query->row_array();
        return $result;
    }

}