<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Installation extends CI_Controller {

    public function Process_Installation() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Process_Installation', array('data' => $data,'method_name' => 'Process_Installation'));
    }
    public function Process_Installation_1() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Process_Installation_1', array('data' => $data,'method_name' => 'Process_Installation_1'));
    }
    public function Payment_Followup() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Payment_Followup', array('data' => $data,'method_name' => 'Payment_Followup'));
    }
    public function Payment_Followup_1() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Payment_Followup_1', array('data' => $data,'method_name' => 'Payment_Followup_1'));
    }
    public function Payment_Followup_2() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Payment_Followup_2', array('data' => $data,'method_name' => 'Payment_Followup_2'));
    }
    public function Payment_Followup_3() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Payment_Followup_3', array('data' => $data,'method_name' => 'Payment_Followup_3'));
    }
    public function Payment_Followup_4() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Payment_Followup_4', array('data' => $data,'method_name' => 'Payment_Followup_4'));
    }
    public function CompletionInvoice() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/CompletionInvoice', array('data' => $data,'method_name' => 'CompletionInvoice'));
    }
    public function Post_Sales_Activity() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('installation/Post_Sales_Activity', array('data' => $data,'method_name' => 'Post_Sales_Activity'));
    }
    public function checklogin() {
        $login_user_id = $this->session->userdata('login_user_id');
        if ($login_user_id <= 0) {
            redirect('login.html');
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */