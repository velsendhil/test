<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 100px !important; }
    .form-horizontal .controls { margin-left: 110px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
</style> 
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Survey Booking</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Survey Administration Charge</label>
                                    <div class="controls">
                                        <select>
                                            <option>No charge</option>
                                            <option>Paid</option>
                                            <option>Collect cash/cheque</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Quote Number</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">To whom</label>
                                    <div class="controls">
                                        <select>
                                            <option>Envirovent</option>
                                            <option>Ecotechnics</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date paid</label>
                                    <div class="controls">
                                        <input type="text" name="btndatepaid" id="btndatepaid" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date of survey booked</label>
                                    <div class="controls">
                                        <input type="text" name="btnDateofsurveybooked" id="btnDateofsurveybooked" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Time of survey booked</label>
                                    <div class="controls">
                                        <input type="text" name="btnTimeofsurveybooked" id="btnTimeofsurveybooked" style="width:100px;" onfocus="Javascript: this.blur();"><img name="imgbtnTimeofsurveybooked" id="imgbtnTimeofsurveybooked" class="ui-datepicker-trigger" src="public/img/calendar.gif" alt="..." title="...">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Survey carried out</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>Cancelled</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible">Reasons why survey not carried out</label>
                                    <div class="controls">
                                        <span style="color:red;">Not clear about dropdown values</span>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible">Description</label>
                                    <div class="controls">
                                        <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>    
<script>
    $(document).ready(function() {
        $("#btndatepaid").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnDateofsurveybooked").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnTimeofsurveybooked").timepicker({ 
            'timeFormat': 'H:i:s',
            buttonImage: "public/img/calendar.gif"
        });
        $("#btnDateofrebooksurvey").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnTimeofrebooksurvey").timepicker({
            'timeFormat': 'H:i:s',
            buttonImage: "public/img/calendar.gif"
        });
        $('#imgbtnTimeofsurveybooked').on('click', function(){
            $('#btnTimeofsurveybooked').timepicker('show');
        });
        $('#imgbtnTimeofrebooksurvey').on('click', function(){
            $('#btnTimeofrebooksurvey').timepicker('show');
        });
    });
</script>        