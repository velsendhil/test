<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projectreports extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function dashboard() {
        $this->checklogin();
        $this->load->view('projectreports/dashboard', array('method_name' => 'dashboard'));
    }
    
    public function checklogin() {
        $login_user_id = $this->session->userdata('login_user_id');
        if ($login_user_id <= 0) {
            redirect('login.html');
        }
    }
    
    public function accountsetting() {
        $this->checklogin();
        $this->load->view('projectreports/accountsetting', array('method_name' => 'accountsetting'));
    }
    
    public function sitehelp() {
        $this->checklogin();
        $this->load->view('projectreports/sitehelp', array('method_name' => 'sitehelp'));
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */