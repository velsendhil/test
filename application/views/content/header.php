<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>ECOTECHNIC SOLUTIONS LIMITED</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="public/css/bootstrap.min.css" rel="stylesheet">
<link href="public/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="public/css/font-awesome.css" rel="stylesheet">
<link href="public/css/style.css" rel="stylesheet">
<link href="public/css/pages/dashboard.css" rel="stylesheet">
<link href="public/css/jquery-ui.css" rel="stylesheet">
<link href="public/css/jquery.timepicker.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<style type="text/css">
    .dropdown ul {
        top :-50px;
    }
    .navbar .dropdown-menu:before {
        top: 55px;
        border-bottom:0px;
    }
    .navbar .dropdown-menu:after {
        border-bottom:0px;
    }
    /*{
        border-bottom: 6px solid #ffffff;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        content: "";
        display: inline-block;
        left: 10px;
        position: absolute;
        top: -6px;
    }*/
    .dropdown ul {
        top: 60px;
    }
</style>
</head>
<body>
<?php
    //session_start();
    //print_r($this->session->userdata["name"]);
?>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container" style="width:100% !important;">
      <ul class="mainnav" style="width:100% !important;">
        <!--<li class="active"><a href="dashboard.html"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>-->
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Forms</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="profile.html"><span>My Profiles</span></a></li>
            <li><a href="register.html"><span>New User</span></a></li>
            <li><a href="usergroup.html"><span>User Group</span></a></li>
            <li><a href="add_item.html"><span>Item Add</span></a></li>
            <li><a href="postal.html"><span>Postal Master</span></a></li>
            <li><a href="itemtype.html"><span>Item Type master</span></a></li>
            <li><a href="territoty.html"><span>Territory Master</span></a></li>
            <li><a href="cost_detail.html"><span>Cost Calculation</span></a></li>
            <li><a href="payment.html"><span>Payment Method</span></a></li>
            <li></li>
            <li></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Lead Stage</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="Primary_Lead_Process.html"><span>Customer Information</span></a></li>
            <li><a href="Lead_Intial.html"><span>Initial lead</span></a></li>
            <li><a href="Lead_Followup_1.html"><span>Lead followup 1st</span></a></li>
            <li><a href="Lead_Followup_2.html"><span>Lead followup 2nd</span></a></li>
            <li><a href="Lead_Followup_3.html"><span>Lead followup 3rd</span></a></li>
            <li><a href="Lead_Followup_4.html"><span>Lead followup 4th</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Survey Stage</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="Survey_Book.html"><span>Survey Stage</span></a></li>
            <li><a href="Survey_rebook.html"><span>Survey Re-Booking</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Quote Stage</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="Quote_Production.html"><span>Quote Production</span></a></li>
            <li><a href="Quote_Cost.html"><span>Quote Costs</span></a></li>
            <li><a href="Quote_Followup_1.html"><span>Quote Followup 1st</span></a></li>
            <li><a href="Quote_Followup_2.html"><span>Quote Followup 2nd</span></a></li>
            <li><a href="Quote_Followup_3.html"><span>Quote Followup 3rd</span></a></li>
            <li><a href="Quote_Followup_4.html"><span>Quote Followup 4th</span></a></li>
            <li><a href="Discount_1.html"><span>Discount 1</span></a></li>
            <li><a href="Discount_2.html"><span>Discount 2</span></a></li>
            <li><a href="Discount_3.html"><span>Discount 3</span></a></li>
            <li><a href="Price_Increase.html"><span>Price Increase</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Sale</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="OrderAgreed_Costs.html"><span>Order Agreed - Costs</span></a></li>
            <li><a href="Quote_Cost.html"><span>Quote Costs</span></a></li>
            <li><a href="Discount_1.html"><span>Discount 1</span></a></li>
            <li><a href="Discount_2.html"><span>Discount 2</span></a></li>
            <li><a href="Discount_3.html"><span>Discount 3</span></a></li>
            <li><a href="Price_Increase.html"><span>Price Increase</span></a></li>
            <li><a href="ordercancel.html"><span>Cancellation of Order</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Deposit Payment</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="Quote_Production.html"><span>Initial Deposit Payment</span></a></li>
            <li><a href="Quote_Production.html"><span>Deposit Payment Follow Up 1</span></a></li>
            <li><a href="Quote_Followup_3.html"><span>Deposit Payment Follow Up 2</span></a></li>
            <li><a href="Quote_Followup_3.html"><span>Deposit Payment Follow Up 3</span></a></li>
            <li><a href="Quote_Followup_3.html"><span>Deposit Invoice</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Installation</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="Process_Installation.html"><span>Installation Booking</span></a></li>
            <li><a href="Payment_Followup.html"><span>Actual Installation 1</span></a></li>
            <li><a href="Process_Installation_1.html"><span>Installation Re-Booking</span></a></li>
            <li><a href="Payment_Followup_2.html"><span>Actual Installation 2</span></a></li>
            <li><a href="Payment_Followup_3.html"><span>Fully Installed Costs</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Final Payment</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="Process_Installation.html"><span>Initial Payment Follow Up</span></a></li>
            <li><a href="Payment_Followup.html"><span>Payment Follow Up 1</span></a></li>
            <li><a href="Payment_Followup_1.html"><span>Payment Follow Up 2</span></a></li>
            <li><a href="Payment_Followup_2.html"><span>Payment Follow Up 3</span></a></li>
            <li><a href="Payment_Followup_3.html"><span>Payment Follow Up 4</span></a></li>
            <li><a href="Payment_Followup_4.html"><span>Payment Follow Up 5</span></a></li>
            <li><a href="CompletionInvoice.html"><span>Completion Invoice</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Post Sales</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <!--<li><a href="#"><span>Testimonials and Certification</span></a></li>-->
            <li><a href="#"><span>Testimonials</span></a></li>
            <li><a href="#"><span>Certification</span></a></li>
            <li><a href="Reconciliation.html"><span>Invoice Receipt and Reconciliation from Envirovent</span></a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Post Sales Activity</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="InvoiceReceipt.html"><span>Reconciliation of Envirovent Payments</span></a></li>
            <li><a href="commissionpayment.html"><span>Commission payments to third parties</span></a></li>
          </ul>
        </li>
         <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Faults and Servicing</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="returnscredits.html"><span>Returns and Credits</span></a></li>
            <li><a href="WarrantyCallOut.html"><span>Warranty Call Out</span></a></li>
            <li><a href="Servicing.html"><span>Servicing</span></a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">