<?php include_once(MSTPATH.'header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="register.html">
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Sign Up</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="firstname">First name</label>
                                        <div class="controls"><input class="span12" id="firstname" name="firstname" type="text" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="lastname">Last name</label>
                                        <div class="controls"><input class="span12" id="lastname" name="lastname" type="text" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="username">Username</label>
                                        <div class="controls"><input class="span12" id="username" name="username" type="text" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="email">E-mail</label>
                                        <div class="controls"><input class="span12" id="email" name="email" type="email" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="password">Password</label>
                                        <div class="controls"><input class="span12" id="password" name="password" type="password" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="confirm_password">Confirm password</label>
                                        <div class="controls"><input class="span12" id="confirm_password" name="confirm_password" type="password" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>