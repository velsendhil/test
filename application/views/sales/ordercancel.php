<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Order Cancel</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <!--<div class="span6">-->
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Reason for order to be cancelled</label>
                                        <div class="controls">
                                            <select name="selReasonforordertobecancelled" id="selReasonforordertobecancelled">
                                                <option value="1">Denied consent to Drill wall</option>
                                                <option value="2">Customer Changes mind</option>
                                                <option value="3">Install Related Issue</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls">
                                            <select name="selReasonforordertobecancelled" id="selReasonforordertobecancelled">
                                                <option value="1">Freeholder Refused Permission</option>
                                                <option value="2">Planning consent refused - listed building</option>
                                                <option value="3">Planning consent refused - conservation area</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls">
                                            <select name="selReasonforordertobecancelled" id="selReasonforordertobecancelled">
                                                <option value="1">Sold at survey - within cooling off period.</option>
                                                <option value="2">Tenant oposition</option>
                                                <option value="3">Decided to sell property</option>
                                                <option value="3">Has decided to do other works first</option>
                                                <option value="3">Personal / financial Issues</option>
                                                <option value="3">Other - Specify</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group" style="width: 810px;">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls">
                                            <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls">
                                            <select name="selReasonforordertobecancelled" id="selReasonforordertobecancelled">
                                                <option value="1">Part of solution can't be installed</option>
                                                <option value="2">Customer doesn't want part solution</option>
                                                <option value="3">EV can't supply an item within a reasonable time</option>
                                                <option value="3">Find cost of fitting tile vent / window opening prohibitively expensive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Ammendment Input on Azura</label>
                                        <div class="controls">
                                            <input type="text" name="btnAmmendmentInputonAzura" id="btnAmmendmentInputonAzura" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Administration fee charged</label>
                                        <div class="controls">
                                            <select>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls">
                                            <select>
                                                <option>abort fee</option>
                                                <option>processing charge</option>
                                                <option>Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Amount</label>
                                        <div class="controls"><input type="text" style="width:100px;"></div>
                                    </div>
                                    <div style="width:100%;float:left;">&nbsp;</div>
                                    <div class="control-group">
                                        <label class="control-label" for="personresponsible">Administration Charge Payment received</label>
                                        <div class="controls">
                                            <input type="text" name="btnAdministrationChargePaymentreceived" id="btnAdministrationChargePaymentreceived" style="width:100px;" readonly="readonly">
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>
                                    <!--<div class="control-group">
                                        <label class="control-label" for="personresponsible"></label>
                                        <div class="controls"><span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>
                                    </div>-->
                                <!--</div>-->
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>
<script>
    $(document).ready(function() {
        $("#btnAmmendmentInputonAzura").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
        $("#btnAdministrationChargePaymentreceived").datepicker({
            showOn: "both",
            buttonImage: "public/img/calendar.gif",
            buttonImageOnly: true,
            /*minDate: -0,*/
            maxDate: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm-dd-yy'
        });
    });
</script>    