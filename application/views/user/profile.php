<?php include_once(MSTPATH.'header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>My Profile</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="firstname">First name</label>
                                        <div class="controls">
                                            <input class="span12" id="firstname" name="firstname" type="text" style="width: 300px;" value="<?php echo $data['firstname']; ?>"/>
                                            <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                        </div>

                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="lastname">Last name</label>
                                        <div class="controls">
                                            <input class="span12" id="lastname" name="lastname" type="text"  style="width: 300px;" value="<?php echo $data['lastname']; ?>"/>
                                            <span data-original-title="Last name is mandatory" data-placement="top" data-toggle="tooltip" class="btn-action single glyphicons circle_question_mark" style="margin: 0;"><i></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="username">Username</label>
                                        <div class="controls"><input class="span12" id="username" name="username" type="text" value="<?php echo $data['username']; ?>" style="width: 300px;"/></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="email">E-mail</label>
                                        <div class="controls"><input class="span12" id="email" name="email" type="email" value="<?php echo $data['email']; ?>" readonly style="width: 300px;"/></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="gender">Gender</label>
                                        <div class="controls">
                                            <select name="gender" id="gender" style="width:184px;">
                                                <option value="Male" <?php if ($data['gender'] == 'Male') echo 'selected'; ?>>Male</option>
                                                <option value="Female" <?php if ($data['gender'] == 'Female') echo 'selected'; ?>>Female</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="age">Age</label>
                                        <div class="controls">
                                            <input type="text" name='age' id="age" class="span12" placeholder="25" value="<?php echo $data['age']; ?>" style="width: 300px;"></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="about_me">About Me</label>
                                        <div class="controls"><textarea name="about_me" id="about_me" style="width: 303px;height:100px;"><?php echo $data['about_me']; ?></textarea></div>
                                    </div>
                                    <?php if ($data['image'] != '' && file_exists('public/upload/' . $data['image'])) { ?>
                                        <div class="control-group">
                                            <label class="control-label">Image Preview</label>
                                            <div class="controls"><img src="<?php echo 'public/upload/' . $data['image']; ?>" alt="imagefile" style="height:100px;weight:100px;border-radius: 10px;" /></div>
                                        </div>
                                    <?php } ?>

                                    <div class="control-group">
                                        <label class="control-label">Image Upload</label>
                                        <div class="controls"><input class="span12" type="file" name="imagefile" id="imagefile" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>            