<?php include_once('header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="add_territoty" method="post" autocomplete="off" action="add_territoty.html">
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i><?php echo $mode;?> Territory</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="name">Name</label>
                                        <div class="controls"><input class="span12" id="typename" name="typename" type="text" /></div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="name">Description</label>
                                        <div class="controls"><textarea name="description" id="description" style="width: 310px;height:80px;"></textarea></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>