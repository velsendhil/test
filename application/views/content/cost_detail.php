<?php include_once('header.php'); ?>
<div id="content" style="min-width:auto;overflow: auto;">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="add_item" method="post" autocomplete="off" action="cost_detail.html">
            <div class="widget widget-gray widget-body-white">
                <div class="widget-head"><h4 class="heading">COST DETAILS</h4></div>
                <div style="padding: 10px 0;" class="widget-body">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th class="center">No.</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>Rate</th>
                                <th>Quantity</th>
                                <th>Net Cost to Customer</th>
                                <th>VAT</th>
                                <th>Gross Cost to Customer</th>
                                <th>Labour</th>

                                <th>Parts Cost</th>
                                <th>MSF</th>
                                <th>Net Cost to</th>
                                <th>VAT</th>
                                <th>Gross Cost to</th>
                                <th>Net Profit to</th>
                                <th>VAT</th>
                                <th>Gross Profit to</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="center">Product 1</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td class="center">Product 2</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td class="center">Product 3</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td class="center">Product 4</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td class="center">Product 5</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td class="center">Product 6</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td class="center">Product 7</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>

                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                            <tr>
                                <td colspan="17">
                                    <div>&nbsp;</div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><div>&nbsp;</div></td>
                                <td colspan="3">Total Cost to Customer</td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>