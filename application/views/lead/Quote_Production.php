<?php include_once(MSTPATH.'header.php'); ?>
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 100px !important; }
    .form-horizontal .controls { margin-left: 110px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Quote Production</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div id="saleofsurvay_a" class="control-group">
                                    <label class="control-label" for="personresponsible">customer type</label>
                                    <div class="controls">&nbsp;</div>
                                </div>    
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Quote required</label>
                                    <div class="controls">
                                        <select name="selQuoterequired" id="selQuoterequired">
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                            <option value="3">Technical advice required</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Reason for no quote required</label>
                                    <div class="controls">
                                        <select name="selReasonfornoquoterequired" id="selReasonfornoquoterequired">
                                            <option value="1">no solution available</option>
                                            <option value="2">Not condensation or mould - damp</option>
                                            <option value="3">Property unsuitable</option>
                                            <option value="4">Customer not interested</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Type of Quote 1</label>
                                    <div class="controls">
                                        <select name="selsaleofsurvay" id="selsaleofsurvay" onchange="Javascript: return saleofsurvay();">
                                            <option value="0">Select type of quote</option>
                                            <option value="1">Supply & Fit</option>
                                            <option value="2">Supply & Fit - Options</option>
                                            <option value="3">Supply Only</option>
                                            <option value="4">Sale at Survey Quote</option>
                                            <option value="5">No quote - order form only</option>
                                            <option value="5">Customised quote-Supply and Fit</option>
                                            <option value="5">Quote paid for by customer</option>
                                            <option value="5">Customised quote-Supply Only</option>
                                            <option value="5">No quote required</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Type of Quote 2</label>
                                    <div class="controls">
                                        <select name="selsaleofsurvay" id="selsaleofsurvay" onchange="Javascript: return saleofsurvay();">
                                            <option value="0">Select type of quote 2</option>
                                            <option value="1">Condensation and Mould</option>
                                            <option value="2">Ventilation</option>
                                            <option value="3">Condensation,Mould and health</option>
                                            <option value="4">Ventilation and health</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Quote Report Charge</label>
                                    <div class="controls">
                                        <select name="selQuoteReportCharge" id="selQuoteReportCharge">
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Quote Number</label>
                                    <div class="controls">
                                        <input type="text" style="width: 84%;"/>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date paid</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">To whom</label>
                                    <div class="controls">
                                        <select name="selTowhom" id="selTowhom">
                                            <option>Envirovent</option>
                                            <option>Ecotechnics</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Money back guarantee drop down box</label>
                                    <div class="controls">
                                        <select name="selQuoteReportCharge" id="selQuoteReportCharge">
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Description of why technical advice needed</label>
                                    <div class="controls">
                                        <input type="text" style="width: 84%;"/>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Source of advice</label>
                                    <div class="controls">
                                        <select name="selSourceofadvice" id="selSourceofadvice">
                                            <option>Surveyor to decide solution</option>
                                            <option>Franchise support team</option>
                                            <option>Technical support team</option>
                                            <option>Installations team</option>
                                            <option>Installations manager/Director</option>
                                            <option>Sam Hawkins</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Contacted</label>
                                    <div class="controls">
                                        <input type="text" name="btnContacted" id="btnContacted" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Outcome</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div id="saleofsurvay_a" class="control-group" >
                                    <label class="control-label" for="personresponsible">Quote required</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Client informed</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date quote drafted by admin</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div id="saleofsurvay_b" class="control-group" >
                                    <label class="control-label" for="personresponsible">Date quote approved by surveyor</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div id="saleofsurvay_b" class="control-group">
                                    <label class="control-label" for="personresponsible">Date quote sent to customer</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div id="saleofsurvay_a" class="control-group" >
                                    <label class="control-label" for="personresponsible">Method</label>
                                    <div class="controls">
                                        <select>
                                            <option>e mail</option>
                                            <option>posted</option>
                                            <option>hand delivered</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date quote entered onto Azura</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>
<script language="Javascript" type="text/javascript">
    function saleofsurvay()
    {
        $("#saleofsurvay_a").hide();
        $("#saleofsurvay_b").hide();
        if ($("#selsaleofsurvay").val() == 5)
        {
            $("#saleofsurvay_a").show();
            $("#saleofsurvay_b").show();
        }
    }
</script>
