<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class payment extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function commissionpayment() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('payment/commissionpayment', array('data' => $data,'method_name' => 'commissionpayment'));
    }
    
    public function checklogin() {
        $login_user_id = $this->session->userdata('login_user_id');
        if ($login_user_id <= 0) {
            redirect('login.html');
        }
    }
    
    public function InvoiceReceiptReconciliationEnvirovent() {
        $this->checklogin();
        $data = array();
        $id = '';
        $mode = 'Add';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $data["id"]=0;
        $data["mode"]=$mode;
        $this->load->view('payment/InvoiceReceiptReconciliationEnvirovent', array('data' => $data,'method_name' => 'InvoiceReceiptReconciliationEnvirovent'));
    }
    
    public function ReconciliationEnviroventPayments() {
        $this->checklogin();
        $data = array();
        $id = '';
        $mode = 'Add';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $data["id"]=0;
        $data["mode"]=$mode;
        $this->load->view('payment/ReconciliationEnviroventPayments', array('data' => $data,'method_name' => 'ReconciliationEnviroventPayments'));
    }
    public function CompletionInvoice() {
        $this->checklogin();
        $data = array();
        $id = '';
        $mode = 'Add';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $data["id"]=0;
        $data["mode"]=$mode;
        $this->load->view('payment/CompletionInvoice', array('data' => $data,'method_name' => 'CompletionInvoice'));
    }
    public function returnscredits() {
        $this->checklogin();
        $data = array();
        $id = '';
        $mode = 'Add';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $data["id"]=0;
        $data["mode"]=$mode;
        $this->load->view('payment/returnscredits', array('data' => $data,'method_name' => 'returnscredits'));
    }
    public function WarrantyCallOut() {
        $this->checklogin();
        $data = array();
        $id = '';
        $mode = 'Add';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $data["id"]=0;
        $data["mode"]=$mode;
        $this->load->view('payment/WarrantyCallOut', array('data' => $data,'method_name' => 'WarrantyCallOut'));
    }
    public function Servicing() {
        $this->checklogin();
        $data = array();
        $id = '';
        $mode = 'Add';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $data["id"]=0;
        $data["mode"]=$mode;
        $this->load->view('payment/Servicing', array('data' => $data,'method_name' => 'Servicing'));
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */