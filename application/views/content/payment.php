<?php include_once('header.php'); ?>
<div id="content" style="min-width:auto;overflow: auto;">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="add_item" method="post" autocomplete="off" action="cost_detail.html">
            <div class="widget widget-gray widget-body-white">
                <div class="widget-head"><h4 class="heading">Payment</h4></div>
                <div style="padding: 10px 0;" class="widget-body">
                    <div class="row-fluid">
                        <div class="span9">
                            <div class="control-group">
                                <label class="control-label" for="firstname" style="width: 200px;margin-right: 10px;">Date Email Confirmation Received from Customer </label>
                                <div class="controls" style="margin-top: 6px;">
                                    2014-04-30
                                </div>

                            </div>
                            <div class="control-group">
                                <label class="control-label" for="lastname" style="width: 200px;margin-right: 10px;">Date Invoice sent to the Customer </label>
                                <div class="controls" style="margin-top: 6px;">
                                    2014-04-30
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="password" style="width: 200px;margin-right: 10px;">Type of Payment </label>
                                <div class="controls">
                                    <select name="paymet_type" id="paymet_type" style="width:184px;" onChange="hideshow(this.value);">
                                        <option value="">--Select Payment Type--</option>
                                        <option value="1">Deposit</option>
                                        <option value="2">Full Payment</option>
                                        <option value="3">By Agreed Payment Schedule</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- start Deposit selected -->
                    <div id="payment_type_1" style="display: none;">
                        <table class="table table-bordered table-condensed">
                            <tbody>
                                <tr>
                                    <th style="width: 20%;">Type of Payment</th>
                                    <th style="width: 20%;">Description Box</th>
                                </tr>
                                <tr>
                                    <td style="width: 20%;">Deposit</td>
                                    <td style="width: 20%;">50% on order and 50% on Completion of Installation</td>
                                </tr>
                            </tbody>
                        </table>
                        </br></br>
                        <table class="table table-bordered table-condensed">
                            <tbody>
                                <tr>
                                    <th colspan="6" style="text-align: center;">50% - DEPOSIT PAYMENT</th>
                                    <th colspan="6" style="text-align: center;">50% FINAL PAYMENT</th>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:75px;">1st Payment Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Date</td>

                                    <td>2014-04-30</td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>

                                    <td>Balance Amount Due</td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" placeholder="Calculative"></div></td>
                                    <td>Balance Amount Paid  Date</td>
                                    <td>2014-04-30</td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end Deposit selected -->
                    <!-- start Full Payment selected -->
                    <div id="payment_type_2" style="display: none;">
                        <table class="table table-bordered table-condensed">
                            <tbody>
                                <tr>
                                    <th style="width: 20%;">Type of Payment</th>
                                    <th style="width: 20%;">Description Box</th>
                                </tr>
                                <tr>
                                    <td style="width: 20%;">Full Payment</td>
                                    <td style="width: 20%;">100% of Payment</td>
                                </tr>
                            </tbody>
                        </table>
                        </br></br>
                        <table class="table table-bordered table-condensed">
                            <tbody>
                                <tr>
                                    <th colspan="6" style="text-align: center;">FULL PAYMENT</th>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:75px;">Payment Amount Due (100% of Total Amount)</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Date (Date Payment Made by Customer)</td>
                                    <td>2014-04-30</td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end Full Payment selected -->
                    <!-- start By Agreed Payment Schedule selected -->
                    <div id="payment_type_3" style="display: none;">
                        <table class="table table-bordered table-condensed">
                            <tbody>
                                <tr>
                                    <th style="width: 20%;">Type of Payment</th>
                                    <th style="width: 20%;">Description Box</th>
                                </tr>
                                <tr>
                                    <td style="width: 20%;">By Agreed Payment Schedule</td>
                                    <td style="width: 20%;">Drop Don Box (4*2.5%/ 3*33.3%/Other)</td>
                                </tr>
                            </tbody>
                        </table>
                        </br></br>
                        <table class="table table-bordered table-condensed">
                            <tbody>
                                <tr>
                                    <th colspan="10" style="text-align: center;">Details of Payment Schedule</th>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:80px;">1st Payment Amount</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Payment Date</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Calculative)"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Notes)"></div></td>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:80px;">2nd Payment Amount</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Payment Date</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Calculative)"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Notes)"></div></td>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:80px;">3rd Payment Amount</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Payment Date</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Calculative)"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Notes)"></div></td>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:80px;">4th Payment Amount</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Payment Date</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Calculative)"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Notes)"></div></td>
                                </tr>
                                <tr>
                                    <td><div class="row-fluid" style="width:80px;">5th Payment Amount</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Payment Date</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;"></div></td>
                                    <td>Payment Method</td>
                                    <td>
                                        <div class="row-fluid">
                                            <select name="paymet_type" id="paymet_type" style="width:184px;">
                                                <option value="Credit card">Credit card</option>
                                                <option value="Debit card">Debit card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="BACS">BACS</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Calculative)"></div></td>
                                    <td><div class="row-fluid" style="width:75px;">Balance Amount Due</div></td>
                                    <td><div class="row-fluid"><input class="span12" type="text" name="prod" style="width: 80px;" value="(Notes)"></div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end By Agreed Payment Schedule selected -->
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>