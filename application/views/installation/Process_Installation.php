<?php include_once(MSTPATH.'header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <div class="innerLR">
            <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
                <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
                <div class="tab-content" style="padding: 0;">
                    <div id="account-details" class="tab-pane active">
                        <div class="widget widget-2">
                            <div class="widget-head">
                                <h4 class="heading glyphicons edit"><i></i>SALE PROCESS- INSTALLATION </h4>
                            </div>
                            <div class="widget-body" style="padding-bottom: 0;">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Date of Installation Booked</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="control-group">
                                                <label class="control-label" for="personresponsible">Time of Installation Booked</label>
                                                <div class="controls">
                                                    <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                    <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Booking Status</label>
                                            <div class="controls">
                                                <select>
                                                    <option>Provisional</option>
                                                    <option>Confirmed</option>
                                                </select>    
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Installation to be Carried Out By</label>
                                            <div class="controls">
                                                <select>
                                                    <option>Envirovent</option>
                                                    <option>Ecotechnic</option>
                                                </select>    
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Installers Name</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Installation Company Name( If applicable)</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Installation carried out</label>
                                            <div class="controls">
                                                <select>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                </select>    
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">date of rebooked installation</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Time of rebooked installation</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Rebooked installation carried out</label>
                                            <div class="controls">
                                                <select>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                </select>    
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Installation Status</label>
                                            <div class="controls">
                                                <select>
                                                    <option>Complete</option>
                                                    <option>Part Complete</option>
                                                </select>    
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">If Installation is part Complete</label>
                                            <div class="controls">
                                                <select>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                </select>    
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Additional Installation Date</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Additional Installation Time</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Notes on Installation</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="personresponsible">Date of Agreed Post-Installation Payment Follow Up</label>
                                            <div class="controls">
                                                <input type="text" style="width:100px;"><img src="public/img/date-icon.png">
                                                <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>    