<?php include_once(MSTPATH.'header.php'); ?>
<!--<div class="span6">
    This is help page
</div>-->
<!-- /span6 -->
<style>
    input[type="text"] {height:20px;}
    .control-label { width: 120px !important; }
    .form-horizontal .controls { margin-left: 130px;}
    .form-horizontal .control-group { float: left; margin-bottom: 20px; width: 405px; }
    .control-group-title { width: 100%; padding: 15px; font-weight:bold;  float: left;}
    select { width: 190px; }                            
</style>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="profile.html" enctype="multipart/form-data">
            <input id="id" name="id" type="hidden" value="<?php echo $data['id']; ?>" />
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i>Returns and Credits</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Customer Reports Product not working</label>
                                    <div class="controls">
                                        <input type="text" name="btnCustomerReportsProductnotworking" id="btnCustomerReportsProductnotworking" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Product Description</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">ECO2 Loft unit</option>
                                            <option value="">ECO2 Air unit</option>
                                            <option value="">ECO2 Twin unit</option>
                                            <option value="">ECO2 Air Twin unit</option>
                                            <option value="">ECO2 Wall unit</option>
                                            <option value="">Cyclone 230V</option>
                                            <option value="">Cyclone SELV</option>
                                            <option value="">Cyclone In Line with IQ Control</option>
                                            <option value="">HeatSava Range - Specify</option>
                                            <option value="">Cyclone 7 -230V</option>
                                            <option value="">Cyclone 7-SELV</option>
                                            <option value="">Cyclone 7 In Line with IQ Control</option>
                                            <option value="">Cyclone 7 230V with IQ Control</option>
                                            <option value="">Cyclone 7 SELV with IQ Control</option>
                                            <option value="">EFHT2S Range - Specify</option>
                                            <option value="">Aerodri Range - Specify</option>
                                            <option value="">Silent Range - Specify</option>
                                            <option value="">Ecodmev Range - Specify</option>
                                            <option value="">MVHR Range - Specify</option>
                                            <option value="">Other - Specify</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is it an IQ Model</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Installed/Delivery</label>
                                    <div class="controls">
                                        <select>
                                            <option>Installed</option>
                                            <option>Delivery</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Azura Quote Number</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Territory Number</label>
                                    <div class="controls">
                                        <select>
                                            <option>36</option>
                                            <option>40</option>
                                            <option>Other - Specify</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Customer Type</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Company Name</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Customer Details</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">Title</option>
                                            <option value="">First Name</option>
                                            <option value="">Surname</option>
                                            <option value="">Contact Number Primary</option>
                                            <option value="">Contact Number Secondary</option>
                                            <option value="">E mail address</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">If Installed</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">Installation House Number or Name</option>
                                            <option value="">Installation Street 1</option>
                                            <option value="">Installation Street 2</option>
                                            <option value="">Installation Town</option>
                                            <option value="">Installation County</option>
                                            <option value="">Installation Post Code</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">If Delivered</label>
                                    <div class="controls">
                                        <select name="selProductDescription" id="selProductDescription">
                                            <option value="">Delivery House Number or Name</option>
                                            <option value="">Delivery Street 1</option>
                                            <option value="">Delivery Street 2</option>
                                            <option value="">Delivery Town</option>
                                            <option value="">Delivery County</option>
                                            <option value="">delivery Post Code</option>
                                        </select>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Original Survey Date</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Surveyed by</label>
                                    <div class="controls">
                                        &nbsp;
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Supply method</label>
                                    <div class="controls">
                                        <select>
                                            <option>Supply Only</option>
                                            <option>Supply and Fit</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Definition of Fault/Problem</label>
                                    <div class="controls">
                                        <select>
                                            <option>Wrong item dispatched</option>
                                            <option>Tile vent dispatched and not used in installation</option>
                                            <option>Product faulty - Specify</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible"></label>
                                    <div class="controls">
                                        <select>
                                            <option>Cartridge requires replacement</option>
                                            <option>Transformer requires replacement</option>
                                            <option>No power to the unit after checks</option>
                                            <option>Other - Specify</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible"></label>
                                    <div class="controls">
                                        <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Head Office e mailed Date</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Head Office e mailed Person</label>
                                    <div class="controls">
                                        <select>
                                            <option>Greg Dimond</option>
                                            <option>Other - Specify</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible"></label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">RAN Number RAN Number</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Replacement to be sent out</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date dispatch due</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date New Order created on Azura</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">New Azura quote number</div>
                                <div style="width:100%;float:left;">Cost to customer</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Net</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Vat</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Gross</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Payment taken by</label>
                                    <div class="controls">
                                        <select>
                                            <option>Debit card</option>
                                            <option>Credit card</option>
                                            <option>On line payment</option>
                                            <option>BACS</option>
                                            <option>Cheque</option>
                                            <option>Cash</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Lodgement number</label>
                                    <div class="controls">
                                        <input type="text" style="width:100px;">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Replacement to be sent out</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date Return due at Envirovent</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date customer contacted to check order receipt</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is the order complete<br/><span style="color:red;">Not clear about how to display the other parameter</span></label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date customer receives order</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible">Details of missing items</label>
                                    <div class="controls">
                                        <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date dispatch of missing items due</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date customer contacted to check order receipt</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is the order complete</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group" style="width: 730px;">
                                    <label class="control-label" for="personresponsible">Missing items - Specify</label>
                                    <div class="controls">
                                        <textarea rows="3" cols="60" style="width: 590px;"></textarea>
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Is the order received<br/><span style="color:red;">Not clear about how to display the other parameter</span></label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Return Received at Envirovent</label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date Return received at Envirovent</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date customer contacted to check Return of Goods to Envirovent</label>
                                    <div class="controls">
                                        <input type="text" name="btnInitialcontactdate" id="btnInitialcontactdate" style="width:100px;" readonly="readonly">
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Credit Note e mailed and received<br/><span style="color:red;">Not clear about how to display the other parameter</span></label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                                <div style="width:100%;float:left;">&nbsp;</div>
                                <div class="control-group">
                                    <label class="control-label" for="personresponsible">Date Return received at Envirovent<br/><span style="color:red;">Not clear about how to display the other parameter</span></label>
                                    <div class="controls">
                                        <select>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>    
                                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-icon btn-primary glyphicons circle_ok" type="submit"><i></i>Save</button>
                                <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>    
<?php include_once(MSTPATH.'footer.php'); ?>