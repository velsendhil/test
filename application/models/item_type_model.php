<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Item_Type_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->main_table = "item_types";
        $this->primary_key = "id";
        $this->title = "Item Types";
    }

    public function validate() {
        $this->load->library('form_validation');
        // set validate ruls
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'comments',
                'label' => 'Comments',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }

    function insert($data = array(), $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->insert($table, $data);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

    function update($data = array(), $id = '', $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $insertId = $this->db->affected_rows();
        #echo $this->db->last_query();
        return $id;
    }

    function delete($where = '', $table = '') {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->where($where);
        return $this->db->delete($table);
    }

    function checkRecordAlreadyExists($field_name, $field_value, $id, $mode) {
        $exists = false;
        if ($mode == 'Add') {
            $this->db->select($this->primary_key);
            $this->db->where($field_name, $field_value);
            $data = $this->db->get($this->main_table);
            if ($data[0][$this->primary_key] > 0) {
                $exists = true;
            }
        } else if ($mode == 'Edit') {
            $this->db->select($this->primary_key);
            $this->db->where($field_name, $field_value);
            $this->db->where($this->primary_key . " !=", $id);
            $data = $this->db->get($this->main_table);
            if ($data[0][$this->primary_key] > 0) {
                $exists = true;
            }
        }
        return $exists;
    }

    function getRecords($id, $table, $field = "*") {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = array();
        $result = $query->row_array();
        return $result;
    }

    function getAllRecords($table, $field = "*") {
        if ($table == '') {
            $table = $this->main_table;
        }
        $this->db->select($field);
        $this->db->from($table);
        $query = $this->db->get();
        $result = array();
        $result = $query->result_array();
        return $result;
    }

}