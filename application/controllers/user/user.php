<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function home() {
        $this->checklogin();
        $this->load->view('content/home');
    }

    public function checklogin() {
        $login_user_id = $this->session->userdata('login_user_id');
        if ($login_user_id <= 0) {
            redirect('login.html');
        }
    }

    public function login() {
        // LOAD LIBRARIES
        $login_user_id = $this->session->userdata('login_user_id');
        if (isset($login_user_id) && $login_user_id > 0) {
            redirect('profile.html');
            exit;
        }
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('user_model');

        if ($this->input->post('user_name')) {
            $db_login = array();
            $user_name = $this->input->post('user_name');
            $user_pass = $this->input->post('user_pass');
            $db_login = $this->user_model->getAuthenicatedUser($user_name, $user_pass);
            //echo "<pre>";
            //print_r($db_login);
            //exit;
            if ($db_login['success'] == "Yes" && $db_login['Status'] == "Active") {
                $msg = 'Welcome! You have successfully login into our site.';
                $this->session->set_flashdata('message', 'Welcome! You have successfully login into our site.');
                redirect('dashboard.html');
                exit;
            } else if ($db_login['success'] == "No" && $db_login['Status'] == "Inactive") {
                $msg = 'Your account status is Deactivated. Please contact Administrator.';
                $this->session->set_flashdata('message', $msg);
                redirect('login.html');
                exit;
            } else if ($db_login['Status'] == "Inactive") {
                $msg = 'Your account status is Deactivated.';
                $this->session->set_flashdata('message', $msg);
                redirect('login.html');
                exit;
            } else {
                $msg = "You have entered wrong username or password.";
                $this->session->set_flashdata('message', $msg);
                redirect('login.html');
                exit;
            }
        }
        //$this->load->view( 'header' );
        $this->load->view('user/login');
        //$this->load->view( 'footer' );
    }

    function logout() {
        session_destroy();
        $this->session->sess_destroy();
        session_start();
        $this->session->sess_create();
        redirect('login.html');
        exit;
    }

    public function register() {
        $postdata = array();
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('user_model');
        //$this->checklogin();
        if ($this->input->post('firstname')) {
            $postdata = $_POST;
            unset($postdata['confirm_password']);
            $db_login = $this->user_model->insert($postdata, 'users');
            if ($db_login > 0) {
                $postdata['status'] = 'Active';
                $this->user_model->setLoginSession($postdata);
                $this->session->set_flashdata('message', 'You are successfully register.');
                redirect('profile.html');
            } else {
                $this->session->set_flashdata('message', 'You are not register successfully.');
            }
        }

        $this->load->view('user/register', array('method_name' => 'register'));
    }

    public function profile() {
        $postdata = array();
        $data = array();
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('user_model');
        $id = $this->session->userdata('id');
        ;
        $this->checklogin();
        if ($this->input->post('firstname')) {
            $postdata = $_POST;
            $db_login = $this->user_model->update($postdata, $id, 'users');
            if ($db_login > 0) {
                $this->session->set_flashdata('message', 'Your profile saved successfully.');
            } else {
                $this->session->set_flashdata('message', 'Error in save profile..');
            }

            $update_arr = array();
            if (isset($_FILES) && isset($_FILES['imagefile']['name']) && $_FILES['imagefile']['name'] != '') {
                $config = array();
                $config['overwrite'] = TRUE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = 2000;
                $config['upload_path'] = APPPATH . '../public/upload';
                $this->load->library('upload', $config);
                $this->upload->do_upload('imagefile');
                $image_data = $this->upload->data();
                //echo "<pre>";
                //print_r($image_data);exit;

                if (isset($image_data['file_name']) && $image_data['file_name'] != '') {
                    $update_arr['id'] = $id;
                    $update_arr['image'] = $image_data['file_name'];
                    $this->user_model->update($update_arr, $id, 'users');
                }
            }
            redirect('profile.html');
        } else {
            $data = $this->user_model->get($id, 'users');
        }
        $this->load->view('user/profile', array('data' => $data, 'method_name' => 'profile'));
    }

    public function usergroup() {
        $title = 'User Group List';
        $link_arr = array('add' => 'add_usergroup.html');
        $this->load->model('user_group_model');
        $page = 1;
        $data = $this->user_group_model->getAllRecords('user_groups', '*');
        $field_arr = array('id' => 'ID', 'group_name' => 'Group Name', 'group_type' => 'Group Type', 'status' => 'Status');
        //echo "<pre>";
        //print_r($data);exit;
        $this->load->view('user/usergroup', array('data' => $data, 'field_arr' => $field_arr, 'link_arr' => $link_arr, 'title' => $title, 'page' => $page, 'method_name' => 'usergroup'));
    }

    public function add_usergroup() {
        $postdata = array();
        $data = array();
        $id = '';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }

        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('user_group_model');
        $this->checklogin();
        if ($this->input->post('group_name')) {
            $postdata = $_POST;
            $id = $postdata['id'];
            unset($postdata['mode']);
            if ($mode == 'Add') {
                $insert_id = $this->user_group_model->insert($postdata, 'user_groups');
            } else {
                $insert_id = $this->user_group_model->update($postdata, $id, 'user_groups');
            }
            if ($insert_id > 0) {
                $this->session->set_flashdata('message', 'User group data has been saved.');
            } else {
                $this->session->set_flashdata('message', 'User group data could not be saved. Please, try again.');
            }
            // return url....    
            redirect('add_usergroup.html?id=' . $insert_id);
        } else {
            $data = $this->user_group_model->getRecords($id, 'user_groups');
        }
        $this->load->view('user/add_usergroup', array('data' => $data, 'mode' => $mode, 'id' => $id, 'method_name' => 'usergroup'));
    }

    public function registration() {
        $this->load->view('welcome_message');
    }

    public function registration_action() {
        $this->load->view('welcome_message');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */