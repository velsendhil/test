<?php include_once(MSTPATH.'header.php'); ?>
<div id="content">
    <div class="separator"></div>
    <div class="innerLR">
        <form class="form-horizontal" id="validateSubmitForm" method="post" autocomplete="off" action="add_usergroup.html">
            <input type='hidden' name="id" id="id" value="<?php echo $id; ?>">
            <input type='hidden' name="mode" id="mode" value="<?php echo $mode; ?>">
            <div class="tab-content" style="padding: 0;">
                <div id="account-details" class="tab-pane active">
                    <div class="widget widget-2">
                        <div class="widget-head">
                            <h4 class="heading glyphicons edit"><i></i><?php echo $mode; ?> User Group</h4>
                        </div>
                        <div class="widget-body" style="padding-bottom: 0;">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="firstname">Group Name</label>
                                        <div class="controls"><input class="span12" id="group_name" name="group_name" type="text" value="<?php if (isset($data['group_name'])) echo $data['group_name']; ?>" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="lastname">Group Type</label>
                                        <div class="controls">
                                            <select name="group_type" id="group_type" style="width:184px;">
                                                <option value="Admin" <?php if (isset($data['group_type']) && $data['group_type'] == 'Admin') echo 'selected'; ?>>Admin</option>
                                                <option value="Super Admin"  <?php if (isset($data['group_type']) && $data['group_type'] == 'Super Admin') echo 'selected'; ?>>Super Admin</option>
                                                <option value="User" <?php if (isset($data['group_type']) && $data['group_type'] == 'User') echo 'selected'; ?>>User</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
                                <a href="usergroup.html" title="Cancel"><button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once(MSTPATH.'footer.php'); ?>