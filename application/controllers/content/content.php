<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function home() {
        $this->checklogin();
        $this->load->view('content/home', array('method_name' => 'home'));
    }

    public function checklogin() {
        $login_user_id = $this->session->userdata('login_user_id');
        if ($login_user_id <= 0) {
            redirect('login.html');
        }
    }

    public function add_item() {
        $postdata = array();
        $data = array();
        $id = '';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }

        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('item_model');
        $this->load->model('item_type_model');
        $item_data = $this->item_type_model->getAllRecords('item_types', 'id,name');
        // print_r($item_data);exit;
        $this->checklogin();
        if ($this->input->post('name')) {
            $postdata = $_POST;
            $id = $postdata['id'];
            unset($postdata['mode']);
            if ($mode == 'Add') {
                $insert_id = $this->item_model->insert($postdata, 'items');
            } else {
                $insert_id = $this->item_model->update($postdata, $id, 'items');
            }
            if ($insert_id > 0) {
                $this->session->set_flashdata('message', 'Item data has been saved.');
            } else {
                $this->session->set_flashdata('message', 'Item data could not be saved. Please, try again.');
            }
            // return url....    
            redirect('add_item.html?id=' . $insert_id);
        } else {
            $data = $this->item_model->getRecords($id, 'items');
        }
        $this->load->view('content/add_item', array('data' => $data, 'item_data' => $item_data, 'mode' => $mode, 'id' => $id, 'method_name' => 'item'));
    }

    public function itemtype() {
        $title = 'Item Type List';
        $link_arr = array('add' => 'add_itemtype.html');
        $this->load->model('item_type_model');
        $page = 1;
        $data = $this->item_type_model->getAllRecords('item_types', '*');
        $field_arr = array('id' => 'ID', 'name' => 'Name', 'description' => 'Description', 'status' => 'Status');
        //echo "<pre>";
        //print_r($data);exit;
        $this->load->view('content/itemtype', array('data' => $data, 'field_arr' => $field_arr, 'link_arr' => $link_arr, 'title' => $title, 'page' => $page, 'method_name' => 'itemtype'));
    }

    public function add_itemtype() {
        $postdata = array();
        $data = array();
        $id = '';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }

        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('item_type_model');
        $this->checklogin();
        if ($this->input->post('name')) {
            $postdata = $_POST;
            $id = $postdata['id'];
            unset($postdata['mode']);
            if ($mode == 'Add') {
                $insert_id = $this->item_type_model->insert($postdata, 'item_types');
            } else {
                $insert_id = $this->item_type_model->update($postdata, $id, 'item_types');
            }
            if ($insert_id > 0) {
                $this->session->set_flashdata('message', 'Item Type data has been saved.');
            } else {
                $this->session->set_flashdata('message', 'Item Type data could not be saved. Please, try again.');
            }
            // return url....    
            redirect('add_itemtype.html?id=' . $insert_id);
        } else {
            $data = $this->item_type_model->getRecords($id, 'item_types');
        }
        $this->load->view('content/add_itemtype', array('data' => $data, 'mode' => $mode, 'id' => $id, 'method_name' => 'itemtype'));
    }

    public function postal() {
        $title = 'Postal List';
        $link_arr = array('add' => 'add_postal.html');
        $this->load->model('postal_model');
        $page = 1;
        $data = $this->postal_model->getAllRecords('postals', '*');
        $field_arr = array('id' => 'ID', 'postal_code' => 'Postal Code', 'population' => 'Population', 'status' => 'Status');
        //echo "<pre>";
        //print_r($data);exit;
        $this->load->view('content/postal', array('data' => $data, 'field_arr' => $field_arr, 'link_arr' => $link_arr, 'title' => $title, 'page' => $page, 'method_name' => 'postal'));
    }

    public function add_postal() {
        $postdata = array();
        $data = array();
        $id = '';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }

        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('postal_model');
        $this->checklogin();
        if ($this->input->post('postal_code')) {
            $postdata = $_POST;
            $id = $postdata['id'];
            unset($postdata['mode']);
            if ($mode == 'Add') {
                $insert_id = $this->postal_model->insert($postdata, 'postals');
            } else {
                $insert_id = $this->postal_model->update($postdata, $id, 'postals');
            }
            if ($insert_id > 0) {
                $this->session->set_flashdata('message', 'Postal data has been saved.');
            } else {
                $this->session->set_flashdata('message', 'Postal data could not be saved. Please, try again.');
            }
            // return url....    
            redirect('add_postal.html?id=' . $insert_id);
        } else {
            $data = $this->postal_model->getRecords($id, 'postals');
        }
        $this->load->view('content/add_postal', array('data' => $data, 'mode' => $mode, 'id' => $id, 'method_name' => 'postal'));
    }

    public function territory() {
        $title = 'Territory List';
        $link_arr = array('add' => 'add_territory.html');
        $this->load->model('territory_model');
        $page = 1;
        $data = $this->territory_model->getAllRecords('territories', '*');
        $field_arr = array('id' => 'ID', 'name' => 'Name', 'description' => 'Description', 'status' => 'Status');
        //echo "<pre>";
        //print_r($data);exit;
        $this->load->view('content/territory', array('data' => $data, 'field_arr' => $field_arr, 'link_arr' => $link_arr, 'title' => $title, 'page' => $page, 'method_name' => 'territory'));
    }

    public function add_territory() {
        $postdata = array();
        $data = array();
        $id = '';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $id = $_REQUEST['id'];
            $mode = 'Edit';
        } else {
            $mode = 'Add';
        }
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        $this->load->model('territory_model');
        $this->checklogin();
        if ($this->input->post('name')) {
            $postdata = $_POST;
            $id = $postdata['id'];
            unset($postdata['mode']);
            if ($mode == 'Add') {
                $insert_id = $this->territory_model->insert($postdata, 'territories');
            } else {
                $insert_id = $this->territory_model->update($postdata, $id, 'territories');
            }
            if ($insert_id > 0) {
                $this->session->set_flashdata('message', 'Territory data has been saved.');
            } else {
                $this->session->set_flashdata('message', 'Territory data could not be saved. Please, try again.');
            }
            // return url....    
            redirect('add_territory.html?id=' . $insert_id);
        } else {
            $data = $this->territory_model->getRecords($id, 'territories');
        }
        $this->load->view('content/add_territory', array('data' => $data, 'mode' => $mode, 'id' => $id, 'method_name' => 'territory'));
    }

    public function cost_detail() {
        $this->load->view('content/cost_detail');
    }

    public function payment() {
        $this->load->view('content/payment');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */