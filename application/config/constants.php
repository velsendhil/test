<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


//define('URL', 'http://localhost/CodeIgniter/');
//define('URL', 'http://velsendhil.x50x.us/CodeIgniter/');
define('URL', 'http://localhost/');
define('IMG', URL . 'public/img/');
define('CSS', URL . 'public/css/');
define('JS', URL . 'public/js/');
define('MSTPATH','D:/wamp/www/application/views/content/');

function pr($var) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

function pre_exit($var) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    exit;
}
function loaddropdown($dropdowntype)
{
    if ($dropdowntype == "Territory")
    {
        
    }
}
function contacttitlevalues($condition="")
{
    $s = "<option value='0'>Select</option>";
    $s = $s."<option value='1'>Mr</option>";
    $s = $s."<option value='2'>Mrs</option>";
    $s = $s."<option value='3'>Mr & Mrs</option>";
    $s = $s."<option value='4'>Ms</option>";
    $s = $s."<option value='5'>Miss</option>";
    $s = $s."<option value='6'>Dr</option>";
    $s = $s."<option value='7'>Rrev</option>";
    return $s;
}
/* End of file constants.php */
/* Location: ./application/config/constants.php */

/*
 * What are tou doing
 * I am chkradhar testing here
 * 
 */
/*
 * This is changes by sendhil 07/08/2014
 * 
 */