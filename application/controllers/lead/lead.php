<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lead extends CI_Controller {

    public function primarylead() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Primary_Lead', array('data' => $data,'method_name' => 'Primarylead'));
    }
    
    public function LeadIntial() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Lead_Intial', array('data' => $data,'method_name' => 'LeadIntial'));
    }
    public function Lead_Followup_1() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Lead_Followup_1', array('data' => $data,'method_name' => 'Lead_Followup_1'));
    }
    public function Lead_Followup_2() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Lead_Followup_2', array('data' => $data,'method_name' => 'Lead_Followup_2'));
    }
    public function Lead_Followup_3() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Lead_Followup_3', array('data' => $data,'method_name' => 'Lead_Followup_3'));
    }
    public function Lead_Followup_4() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Lead_Followup_4', array('data' => $data,'method_name' => 'Lead_Followup_4'));
    }
    public function Primary_Lead_Process() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->model("lead_model");
        $territory = $this->generateterritory($this->lead_model->getTerritory());
        $data["territory"] = $territory;
        $data["leadreceived"] = $this->generateleadreceived($this->lead_model->getleadreceived());
        $this->load->view('lead/Primary_Lead_Process', array('data' => $data,'method_name' => 'Primary_Lead_Process'));
    }
    public function Survey_Book() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Survey_Book', array('data' => $data,'method_name' => 'Survey_Book'));
    }
    public function Survey_rebook() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Survey_rebook', array('data' => $data,'method_name' => 'Survey_rebook'));
    }
    public function Quote_Followup_1() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Quote_Followup_1', array('data' => $data,'method_name' => 'Quote_Followup_1'));
    }
    public function Quote_Followup_2() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Quote_Followup_2', array('data' => $data,'method_name' => 'Quote_Followup_2'));
    }
    public function Quote_Followup_3() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Quote_Followup_3', array('data' => $data,'method_name' => 'Quote_Followup_3'));
    }
    public function Quote_Followup_4() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Quote_Followup_4', array('data' => $data,'method_name' => 'Quote_Followup_4'));
    }
    public function Discount_1() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Discount_1', array('data' => $data,'method_name' => 'Discount_1'));
    }
    public function Discount_2() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Discount_2', array('data' => $data,'method_name' => 'Discount_2'));
    }
    public function Discount_3() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Discount_3', array('data' => $data,'method_name' => 'Discount_3'));
    }
    public function Price_Increase() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Price_Increase', array('data' => $data,'method_name' => 'Price_Increase'));
    }
    public function Quote_Production() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Quote_Production', array('data' => $data,'method_name' => 'Quote_Production'));
    }
    public function Quote_Cost() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/Quote_Cost', array('data' => $data,'method_name' => 'Quote_Cost'));
    }
    public function OrderAgreed_Costs() {
        $this->checklogin();
        $data = array("id"=>0);
        $this->load->view('lead/OrderAgreed_Costs', array('data' => $data,'method_name' => 'OrderAgreed_Costs'));
    }
    public function checklogin() {
        $login_user_id = $this->session->userdata('login_user_id');
        if ($login_user_id <= 0) {
            redirect('login.html');
        }
    }
    public function generateterritory($dataarray)
    {
        $opt = "<option value='0'>Select Territory</option>";
        foreach($dataarray as $key => $values)
        {
            $opt = $opt."<option value='".$values->id."'>".$values->name."</option>";
        }
        return $opt;
    }
    public function generateleadreceived($dataarray)
    {
        $opt = "<option value='0'>Select Lead Received</option>";
        foreach($dataarray as $key => $values)
        {
            $opt = $opt."<option value='".$values->id."'>".$values->content."</option>";
        }
        return $opt;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */